# ----------------------------------------------------------------------------
# Nom :        ???.pl
# Sujet :      
# Version :    0.1
#
# Auteur :     jessy
# Cr�ation :   13/11/2020
# M�j :        13/11/2020
# ----------------------------------------------------------------------------

PROGRAMME nomduprog ;

Constante mdp : chaine = "654321aZ" ;

VAR inputmdp : chaine ;
	compte : entier = 0;

 

DEBUTPROG
	  
	REPETER
   		Afficher("Tapez le mot de passe : ", CRLF);
   		saisir(inputmdp);
		
		SI comparer(mdp, inputmdp) = 0 ALORS 
   			afficher(CRLF,"BIENVENUE",CRLF);
   		SINON
  		
			compte <- compte + 1 ;
			afficher(CRLF, "MDP incorrect PLUS QUE ", compte - 5 ," TENTATIVE", CRLF ); 

			SI compte = 5 ALORS 
   				afficher(CRLF,"PLUS DE TENTATIVES",CRLF);
  			FINSI
   		FINSI   		
   		
   JUSQU'A CE QUE ( comparer(mdp, inputmdp) = 0) OU (compte = 5);
	
FINPROG

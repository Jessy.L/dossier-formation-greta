PROGRAMME nomduprog ;

VAR choix: caract�re; 
	

DEBUTPROG

	R�p�ter 
	
		afficher("O - ouvrir les volets ", CRLF, "F - Fermer les volets ", CRLF, "P - Programmer les volets ", CRLF, "Q - Quitter ", CRLF);
		saisir(choix);


		CAS choix PARMI

			'O', 'o' : D�but afficher(CRLF ,"Ouvre les volets", CRLF); FIN
			'F', 'f' : D�but afficher(CRLF ,"Ferme les volets", CRLF); FIN			
			'P', 'p' : D�but afficher(CRLF ,"Programme les volets", CRLF); FIN					
			
			d�faut afficher(CRLF ,"ERREUR", CRLF);

		FINCAS	

 	JUSQU'A CE QUE ( choix = 'O' ) OU ( choix = 'o' ) OU ( choix = 'F' ) OU ( choix = 'f' )  OU ( choix = 'P' ) OU ( choix = 'p' ) ; 

FINPROG
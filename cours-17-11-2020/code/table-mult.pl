PROGRAMME nomduprog ;

VAR nombre : entier = 0 ;
	index : entier;  
	max : entier = 0 ; 

DEBUTPROG
	
	afficher("Nombre � calculer : ");
	saisir(nombre);
	
	afficher("Afficher max (Exemple 10 calculs) : ");
	saisir(max);
	
	POUR index <- 1 JUSQU'A max FAIRE
		afficher(nombre, " x ", index, " = ", nombre*index , CRLF);
	FINP
	
FINPROG
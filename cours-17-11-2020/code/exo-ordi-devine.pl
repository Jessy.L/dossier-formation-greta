PROGRAMME nomduprog ;

VAR nombre : entier = 0 ;
	nombreOrdi : entier  = 1; 
	
	limite : entier = 100;
	choix : entier = 0;
	
	max : entier = 0 ;
	min: entier = 0 ;
	
	nbEssai : entier = 10 ;
	coup : entier = 0 ;	
	
DEBUTPROG
InitAl�atoire();

	max <- 10000;
	min <- 0; 
	
	afficher("Tapez votre nombre : ");
	saisir(nombre);
		
	afficher("Tapez une limite max (entre 0 et 100) ");
	saisir(limite);

	afficher("Tapez une limite de coup (d�faut 10)");
	saisir(nbEssai);

	REPETER
		
		nombreOrdi <- al�atoire(limite);
		
		SI (nombreOrdi > min) ET (nombreOrdi < max) ALORS 
						
			
			EffacerEcran ;
			afficher("Votre nombre : ", nombre);
			afficher("Num de l'ordi : ", nombreOrdi);
			afficher(CRLF ,"Tapez ", CRLF ," 1- PLUS", CRLF ," 2- MOINS", CRLF ," 3- EGAL ", CRLF);
			saisir(choix);
			
			Si coup = nbEssai ALORS 
				afficher(CRLF, "L'ordi a  PERDU", CRLF);	
			FINSI
			
			CAS choix PARMI 
				
				1 : DEBUT min <- nombreOrdi ;   FIN
				2 : DEBUT max <- nombreOrdi ;   FIN
				3 : DEBUT afficher(" L'ordi a GAGNEZ "); FIN

			FINCAS

			
			coup <- coup + 1 ; 		
				
			SI coup = nbEssai ALORS 
				afficher(CRLF, "L'ordi a  PERDU", CRLF);	
			FINSI	
						
		FINSI
		 		

	JUSQU'A CE QUE (nombre = nombreOrdi) OU (nbEssai = coup );

	
FINPROG
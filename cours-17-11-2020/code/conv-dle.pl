# ----------------------------------------------------------------------------
# Nom :        ???.pl
# Sujet :      
# Version :    0.1
#
# Auteur :     jessy
# Cr�ation :   13/11/2020
# M�j :        13/11/2020
# ----------------------------------------------------------------------------

PROGRAMME nomduprog ;

CONSTANTE dollar : r�el = 1.10758 ;
		  livre : r�el = 0.85 ;
		  chf : r�el = 1.09 ; 

Var nombre : r�el = 0 ;
	choix : entier = 0 ; 
DEBUTPROG
 	Afficher("Tapez votre somme : ",CRLF ) ;
	saisir(nombre);
 	
 	
 	Afficher("Convertir en ? :",CRLF," 1 - $ ", CRLF , " 2 - �", CRLF , " 3 - CHF", CRLF);
 	saisir(choix);
 	
	CAS choix PARMI 
		1 : DEBUT afficher(CRLF, (nombre * dollar):4:2 , " $", CRLF); FIN
		2 : DEBUT afficher(CRLF, (nombre * livre):4:2 , " �", CRLF); FIN
		3 : DEBUT afficher(CRLF, (nombre * chf):4:2 , " CHF", CRLF); FIN
	FINCAS	
FINPROG

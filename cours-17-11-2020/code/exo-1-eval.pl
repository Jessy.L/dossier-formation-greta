# ----------------------------------------------------------------------------
# Nom :        ???.pl
# Sujet :      
# Version :    0.1
#
# Auteur :     jessy
# Cr�ation :   13/11/2020
# M�j :        13/11/2020
# ----------------------------------------------------------------------------

PROGRAMME nomduprog ;

VAR inputMdp : ShortString ;
	compte : entier = 0;

CONSTANTE motdp = "654321aZ";
	
DEBUTPROG

	REPETER 

		Afficher("Tapez le mot de passe",CRLF ) ;
   		Saisir(inputMdp);
   		compte <- compte + 1 ;
		afficher("MDP incorrect il vous reste : ", compte - 3 ,CRLF);

	JUSQU'A CE QUE (inputMDP = motdp)  OU (compte = 3);

   	SI inputMdp = motdp ALORS 
   		afficher(CRLF,"BONJOUR BIENVENUE",CRLF);
   	FINSI
	
	SI compte = 5 ALORS 
   		afficher(CRLF,"Tu n'as pas l'age",CRLF);
  	FINSI

FINPROG

PROGRAMME nomduprog ;

VAR montant : entier = 0;
	
CONSTANTE infTrenteHuit = 0.15 ;
		  entreThEtInfCinqCent : r�el = 0.28 ;
		  supCinqCent : r�el = 0.31 ;


DEBUTPROG
	
	afficher("Saisir votre montant : ", CRLF);
	saisir(montant);

	SI montant < 38120 ALORS 
		afficher("Impot : ", (infTrenteHuit*montant):4:2, CRLF)
	FINSI
	
	SI ( montant > 38120 ) ET ( montant < 500000 ) ALORS 
		afficher("Impot : ", (entreThEtInfCinqCent*montant):4:2, CRLF)
	FINSI
	
	SI montant > 500000 ALORS 
		afficher("Impot : ", (montant*supCinqCent*montant):4:2, CRLF)
	FINSI
	

FINPROG
# ----------------------------------------------------------------------------
# Nom         : abo.pl
# Sujet       : 
# Version     : 0.1
#
# Auteur      : TPDW
# Cr�ation    : 10/11/2020
# Mise � jour : 10/11/2020
# ----------------------------------------------------------------------------
# source g�n�r� par MoniaOrg version 0.38

Programme abo ; # Nom du programme 

##FORWARDCOMMENT##

VAR		tarifAboMensuel : r�el = 0 ; # D�clare le nom de la variable mais aussi le type de celle-ci sur l'exemple un r�el

DEBUTPROG
	afficher("Saisir le prix de l'abo mensuel : ", CRLF) ; # Va afficher la cha�ne entre les ""
	saisir(tarifAboMensuel) ; # Demande � l'utilisateur de saisir
	afficher(CRLF, "� la ligne") ; # CRLF = Permet de revenir � la ligne
	afficher("le co�t est de : ", tarifAboMensuel:4:2, CRLF) ; #4 = 4 chiffre max a afficher 2 = 2 chiffres apre�s la virgule
FINPROG

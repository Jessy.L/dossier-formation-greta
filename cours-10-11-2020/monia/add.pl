Programme abo ; # Nom du programme 

##FORWARDCOMMENT##

VAR nombre1 : entier = 0 ; # D�clare le nom de la variable mais aussi le type de celle-ci sur l'exemple un r�el
VAR nombre2 : entier = 0 ;

DEBUTPROG
	afficher("tapez 2 nombre � calc")
	saisir(nombre1) ; # Demande � l'utilisateur de saisir
	saisir(nombre2) ; # Demande � l'utilisateur de saisir
	afficher(CRLF, "la somme des 2 est de : ", nombre1 + nombre2) ; # CRLF = Permet de revenir � la ligne
	
FINPROG

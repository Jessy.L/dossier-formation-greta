# ----------------------------------------------------------------------------
# Nom :        ???.pl
# Sujet :      
# Version :    0.1
#
# Auteur :     TPDW
# Cr�ation :   10/11/2020
# M�j :        10/11/2020
# ----------------------------------------------------------------------------

PROGRAMME nomduprog ;

VAR nombre : entier = 0 ;

DEBUTPROG
   afficher("Hello World ! tape un chiffre pour avoir son CARRE",CRLF ) ;
   saisir(nombre);
   afficher(CARRE(nombre), " est le carre du nombre");
   
FINPROG

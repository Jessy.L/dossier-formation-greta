# ----------------------------------------------------------------------------
# Nom :        ???.pl
# Sujet :      
# Version :    0.1
#
# Auteur :     TPDW
# Cr�ation :   10/11/2020
# M�j :        10/11/2020
# ----------------------------------------------------------------------------

PROGRAMME nomduprog ;

VAR nombre : entier = 0 ;
VAR nombre2 : entier = 1 ;
VAR division : entier = 0 ;

DEBUTPROG
   Afficher("hello world !",CRLF ) ;
   Saisir(nombre);
   Afficher("nombre 2");
   Saisir(nombre2);

	SI (nombre2 = 0 ) ALORS 
		afficher("ERREUR Votre nombre 2 = 0")
	SINON
		division <- nombre DIV(nombre2);
		Afficher("La division de ", nombre, " par ", nombre2, " vaut ", division, " et le reste vaut ", nombre MOD(nombre2), CRLF );
	FINSI	   
   
FINPROG

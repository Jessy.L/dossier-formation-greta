Programme adiv ; # Nom du programme 

##FORWARDCOMMENT##

VAR nombre : entier = 0 ; # D�clare le nom de la variable mais aussi le type de celle-ci sur l'exemple un r�el

DEBUTPROG
	
	afficher("Saisir le nombre ", CRLF) ; 
	saisir(nombre) ; 
	afficher(CRLF, "le double : ", nombre*2, CRLF) ; 
	
	SI( nombre MOD(2) = 0) ALORS # si le nombre MODULO 2 (MOD(2)) = 0 ALORS il execute   
		afficher(CRLF,"le nombre ", nombre," est div 2", CRLF);
	SINON # si il ne l'est pas
		afficher(CRLF, "le nombre ", nombre, " n'est pas div 2", CRLF)	;
	FINSI 

FINPROG

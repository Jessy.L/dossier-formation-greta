var input = document.getElementById("saisie")
var recommencer = document.getElementById("recommencer").addEventListener("click", setGame)

var btn = document.getElementById('btn')
btn.addEventListener('click', game)

var message = document.createElement('p')
document.body.appendChild(message)

var stock = document.createElement("p")
document.body.appendChild(stock)

var stockValue = []
var tentative = 0
var nombreAleatoire; 

function game(){
    
    stockValue.push(input.value)

    if(tentative < 10){

        if(nombreAleatoire == Number(input.value)){
    
            message.innerHTML = "GAGNER"
            message.style.backgroundColor = "Green"
            btn.style.display = "none"
            stock.innerHTML = ""
            stockValue = []
            tentative = 0
            return
    
        }else if(nombreAleatoire > input.value){
    
            message.innerHTML = "TROP PETIT"
            stock.innerHTML = "VOS PROPOSITIONS : " + stockValue
            tentative++
    
        }else if(nombreAleatoire < input.value){
    
            message.innerHTML = "TROP GRAND"
            stock.innerHTML =  "VOS PROPOSITIONS : " + stockValue
            tentative++

        }

    }else{
        message.innerHTML = "PERDU"
        btn.style.display = "none"
        message.style.backgroundColor = "red"
        stock.innerHTML = ""
        stockValue = []
        tentative = 0
        return
    }

}

function setGame(){
    nombreAleatoire = Math.floor(Math.random() * 100);
    btn.style.display = ""
    message.innerHTML = ""
    stock.innerHTML = ""
    stockValue = []
    tentative = 0
    return 
}

setGame()
var NomEleves = [ "Dupont" ,"Durand", "Petit", "Martin", "Legrand", "Lacroix", "Legros", "Lenaim"]
var Notes = [[10, 12, 18, 5, 9, 13, 16, 19 ], [13,11,14,7,14,16,12,10], [9 ,14, 14, 12, 8, 20 ,13 ,13]];

const moyenne = Notes.length
var stock_moyenne = 0

var tableau = document.getElementById("tableau")
tableau.style.border = "1px solid black"

var tableau2 = document.getElementById("tableau2")
tableau2.style.border = "1px solid black"

var all_min_note = []
var all_max_note = []


//_____________________________________________________________
// Afficher le tableau avec les notes 
//_____________________________________________________________


function afficherTab(){
    for(i = 0; i < NomEleves.length; i++){

        var tr = document.createElement("tr")
        tableau.appendChild(tr)
        
        var td = document.createElement('td')
        tr.appendChild(td)
        td.innerHTML = NomEleves[i]
        td.style.border = "1px solid black"

        for(j = 0; j < 3; j++){

            var td2 = document.createElement('td')
            tr.appendChild(td2)
            td2.style.border = "1px solid black"
            td2.innerHTML = Notes[j][i]
            
        }
    }
}

//_____________________________________________________________
// Afficher le tableau avec les moyenne 
//_____________________________________________________________


function afficherTabMoyenne(){

    for(var i = 0 ; i < NomEleves.length; i++){

        var tr = document.createElement("tr")
        tableau2.appendChild(tr)
        
        var td = document.createElement('td')
        tr.appendChild(td)
        td.innerHTML = NomEleves[i]
        td.style.border = "1px solid black"

        var td_moy = document.createElement('td')
        tr.appendChild(td_moy)
        td_moy.style.border = "1px solid black";

        for(var j = 0; j < Notes.length; j++){
            stock_moyenne += Notes[j][i]
        }

        td_moy.innerHTML = ((stock_moyenne)/moyenne).toFixed(2)
        stock_moyenne = 0
    }
}


//_____________________________________________________________
// function de calcul note min et max
//_____________________________________________________________



function NoteMin(){

    var min;
    
    for( i = 0; i < Notes.length;i++){
        min = Notes[i][0]
        for(j = 0; j < Notes[i].length; j++){
            
            if(min > Notes[i][j]){
                min = Notes[i][j]
            }

        }
        all_min_note.push(min)
    }
    afficherNoteMin(all_min_note)
}


function NoteMax(){

    var max;
    
    for( i = 0; i < Notes.length;i++){
        max = Notes[i][0]
        for(j = 0; j < Notes[i].length; j++){
            
            if(max < Notes[i][j]){
                max = Notes[i][j]
            }

        }
        all_max_note.push(max)
    }
    afficherNoteMax(all_max_note)
}

//_____________________________________________________________
// Afficher mes notes MAX ET MIN
//_____________________________________________________________


function afficherNoteMax(all_max_note){

    max = all_max_note[0]

    for(i = 0; i < all_max_note.length; i++){
            
        if(max < all_max_note[i]){
            max = all_max_note[i]
        }

    }

    var p = document.createElement("p")
    document.body.appendChild(p)
    p.innerHTML = "La note la plus grande de la classe : " + max
}


function afficherNoteMin(all_min_note){

    min = all_min_note[0]

    for(i = 0; i < all_min_note.length; i++){
            
        if(min > all_min_note[i]){
            min = all_min_note[i]
        }

    }

    var p = document.createElement("p")
    document.body.appendChild(p)
    p.innerHTML = "La note la grande petite de la classe : " + min
}



// set les function 

afficherTab()
afficherTabMoyenne()
NoteMin()
NoteMax()
-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 14 jan. 2021 à 14:31
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestioncourses`
--

-- --------------------------------------------------------

--
-- Structure de la table `arrivee`
--

DROP TABLE IF EXISTS `arrivee`;
CREATE TABLE IF NOT EXISTS `arrivee` (
  `idArrivee` int(11) NOT NULL AUTO_INCREMENT,
  `Temps` datetime NOT NULL,
  PRIMARY KEY (`idArrivee`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `arrivee`
--

INSERT INTO `arrivee` (`idArrivee`, `Temps`) VALUES
(1, '2021-01-05 11:57:22'),
(2, '2021-01-04 17:43:39'),
(3, '2021-01-03 11:13:47');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `IdCatégorie` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) NOT NULL,
  `AgeMini` int(11) NOT NULL,
  `AgeMaxi` int(11) NOT NULL,
  `DistanceMax` int(11) NOT NULL,
  PRIMARY KEY (`IdCatégorie`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`IdCatégorie`, `Nom`, `AgeMini`, `AgeMaxi`, `DistanceMax`) VALUES
(9, 'Minime', 14, 15, 5000),
(10, 'Cadet', 16, 17, 15),
(11, 'Junior', 18, 19, 25),
(12, 'Senior', 23, 39, 0);

-- --------------------------------------------------------

--
-- Structure de la table `club`
--

DROP TABLE IF EXISTS `club`;
CREATE TABLE IF NOT EXISTS `club` (
  `IdClub` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) NOT NULL,
  `Adresse` varchar(45) NOT NULL,
  `CodePostal` int(11) NOT NULL,
  `Ville` varchar(45) NOT NULL,
  PRIMARY KEY (`IdClub`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `club`
--

INSERT INTO `club` (`IdClub`, `Nom`, `Adresse`, `CodePostal`, `Ville`) VALUES
(1, 'NomClub1', 'AdresseClub1', 11111, 'VilleClub1'),
(2, 'NomClub2', 'AdresseClub2', 22222, 'VilleClub2'),
(3, 'NomClub3', 'AdresseClub2', 33333, 'VilleClub3');

-- --------------------------------------------------------

--
-- Structure de la table `coureur`
--

DROP TABLE IF EXISTS `coureur`;
CREATE TABLE IF NOT EXISTS `coureur` (
  `IdCoureur` int(11) NOT NULL AUTO_INCREMENT,
  `Club_IdClub` int(11) NOT NULL,
  `Categorie_IdCategorie` int(11) NOT NULL,
  `Nom` varchar(45) NOT NULL,
  `Prenom` varchar(45) NOT NULL,
  `Adresse` varchar(45) NOT NULL,
  `CodePostal` int(11) NOT NULL,
  `Ville` varchar(45) NOT NULL,
  `DateNaissance` date NOT NULL,
  `Sexe` char(1) NOT NULL,
  PRIMARY KEY (`IdCoureur`),
  KEY `Club_Coureur` (`Club_IdClub`),
  KEY `Categorie_IdCategorie` (`Categorie_IdCategorie`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `coureur`
--

INSERT INTO `coureur` (`IdCoureur`, `Club_IdClub`, `Categorie_IdCategorie`, `Nom`, `Prenom`, `Adresse`, `CodePostal`, `Ville`, `DateNaissance`, `Sexe`) VALUES
(1, 1, 10, 'NomC1', 'PrenomC1', 'AdresseC1', 11111, 'VilleC1', '2005-01-01', 'M'),
(2, 1, 12, 'NomC2', 'PrenomC2', 'AdresseC2', 22222, 'VilleC2', '1987-02-10', 'F'),
(3, 2, 11, 'NomC3', 'PrenomC3', 'AdresseC3', 33333, 'VilleC3', '2001-03-17', 'M');

-- --------------------------------------------------------

--
-- Structure de la table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `idCourse` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) NOT NULL,
  `Date` date NOT NULL,
  `Distance` int(11) NOT NULL,
  `HeureDepart` datetime NOT NULL,
  PRIMARY KEY (`idCourse`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `course`
--

INSERT INTO `course` (`idCourse`, `Nom`, `Date`, `Distance`, `HeureDepart`) VALUES
(1, 'NomC1', '2021-01-05', 14, '2021-01-05 10:15:28'),
(2, 'NomC2', '2021-01-04', 21, '2021-01-04 15:31:54'),
(3, 'NomC3', '2021-01-03', 7, '2021-01-03 09:46:01');

-- --------------------------------------------------------

--
-- Structure de la table `inscription`
--

DROP TABLE IF EXISTS `inscription`;
CREATE TABLE IF NOT EXISTS `inscription` (
  `idInscription` int(11) NOT NULL AUTO_INCREMENT,
  `Coureur_idCoureur` int(11) NOT NULL,
  `Transpondeur_idTranspondeur` int(11) NOT NULL,
  `Course_idCourse` int(11) NOT NULL,
  `NumDossard` int(11) NOT NULL,
  PRIMARY KEY (`idInscription`),
  KEY `Coureur_Inscription` (`Coureur_idCoureur`),
  KEY `Transpondeur_Inscription` (`Transpondeur_idTranspondeur`),
  KEY `Course_Inscription` (`Course_idCourse`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `inscription`
--

INSERT INTO `inscription` (`idInscription`, `Coureur_idCoureur`, `Transpondeur_idTranspondeur`, `Course_idCourse`, `NumDossard`) VALUES
(1, 1, 1, 1, 13),
(2, 2, 2, 2, 21),
(4, 3, 3, 3, 57);

-- --------------------------------------------------------

--
-- Structure de la table `transpondeur`
--

DROP TABLE IF EXISTS `transpondeur`;
CREATE TABLE IF NOT EXISTS `transpondeur` (
  `idTranspondeur` int(11) NOT NULL AUTO_INCREMENT,
  `Perdu` tinyint(1) NOT NULL,
  PRIMARY KEY (`idTranspondeur`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `transpondeur`
--

INSERT INTO `transpondeur` (`idTranspondeur`, `Perdu`) VALUES
(1, 0),
(2, 0),
(3, 1);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `coureur`
--
ALTER TABLE `coureur`
  ADD CONSTRAINT `Club_Coureur` FOREIGN KEY (`Club_IdClub`) REFERENCES `club` (`IdClub`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `coureur_ibfk_1` FOREIGN KEY (`Categorie_IdCategorie`) REFERENCES `categorie` (`IdCatégorie`);

--
-- Contraintes pour la table `inscription`
--
ALTER TABLE `inscription`
  ADD CONSTRAINT `Coureur_Inscription` FOREIGN KEY (`Coureur_idCoureur`) REFERENCES `coureur` (`IdCoureur`) ON DELETE CASCADE,
  ADD CONSTRAINT `Course_Inscription` FOREIGN KEY (`Course_idCourse`) REFERENCES `course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Transpondeur_Inscription` FOREIGN KEY (`Transpondeur_idTranspondeur`) REFERENCES `transpondeur` (`idTranspondeur`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

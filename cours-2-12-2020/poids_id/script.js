var taille = parseFloat(prompt("Entrez votre taille : "))
var poids = parseFloat(prompt("Votre poids : "))
var genre = parseFloat(prompt("Votre genre : 1-Homme 2-Femme : "))

function calculPoidsDevine(taille, genre){
    if(genre == 1){
        var devine = 50 + 2.3 * ((taille / 2.54) -60)
    } else if(genre == 2){
        var devine = 45.5 + 2.3 * ((taille / 2.54) -60)
    }
    document.write("DEVINE : poids id : " + devine + "<br><br>")
}

function calculPoidsLorentz(taille, genre){
    if(genre == 1){
        var lorentz = (taille - 100) - ((taille - 150)/4)  -  7.2 
    } else if(genre == 2){
        var lorentz = (taille - 100) - ((taille - 150)/2.5)  -  7.2 
    }
    document.write("LORENTZ : poids id : " + lorentz + "<br><br>")
}

function calculIMC(taille, poids){
    var imc = poids / Math.pow(taille*0.01, 2)
    console.log(imc)
    document.write("Votre imc est de  : " + Number(imc).toFixed(2))
}

calculPoidsDevine(taille, genre)
calculPoidsLorentz(taille, genre)
calculIMC(taille, poids)
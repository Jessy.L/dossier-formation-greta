<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>La pisciculture PHP</title>
        <link rel="stylesheet" href="css/styles.css" />
    </head>

    <body>
        <div id="conteneur">
            <header>
                <h1>La pisciculture PHP</h1>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php?page=index">Accueil</a></li>
                    <li><a href="index.php?page=bassins">Les bassins</a></li>
                    <li><a href="index.php?page=arcenciel">La truite arc-en-ciel</a></li>
                </ul>
            </nav>
            
           
            <?php
            if(!isset($_GET['page']) || !file_exists("asset/".$_GET['page'].'.php') ){

                require_once('asset/accueil.php');
                
            }else{

                require_once("asset/". $_GET['page'] .".php");

            }
            ?>

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>




<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>La pisciculture PHP</title>
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
        <div id="conteneur">
            <header>
                <h1>La pisciculture PHP</h1>
            </header>

            <?php include("nav.php") ?>
            <section>
                <article>                
                    <h1>Les bassins</h1>
                    <p></p>
                </article>
            </section>

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>La pisciculture PHP</title>
        <link rel="stylesheet" href="css/styles.css" />
    </head>

    <body>
        <div id="conteneur">
            <header>
                <h1>La pisciculture PHP</h1>
            </header>

            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="bassins.php">Les bassins</a></li>
                    <li><a href="arcenciel.php">La truite arc-en-ciel</a></li>
                    <li><a href="temp.php">La temp</a></li>
                </ul>
            </nav>

            <!--INSERT PHP-->

            <footer>
                <p>Copyright TruitesPHP - Tous droits réservés - 
                    <a href="#">Contact</a></p>
            </footer>
        </div>    
    </body>
</html>
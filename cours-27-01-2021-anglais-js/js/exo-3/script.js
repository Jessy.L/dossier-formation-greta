var departements = ["Alpes-maritimes", "Jura", "Territoire de Belfort", "Doubs", "Haute-Marnes", "Haute Saone", "Guyane", "Meuse", "Hautes-Alpes", "Var",
	"Ardennes", "Cher", "Bouches-du-Rhone", "Meurthe-et-Moselle", "Nievre", "Vaucluse", "Soane-et-Loire", "Drome", "Allier", "Cote-d'Or",
	"Moselle", "Deux-Sèvres", "Haut-Rhin", "Haute_Loire", "Marne", "Vosges", "Isère", "Haute-Savoie", "Somme", "Rhone",
	"Ariege", "Loire", "Ain", "Val-de-Marne", "Puy-de-Dome", "Ardeche", "Val-d'Oise", "Indre", "Savoie", "Alpes-de-Haute-Provence",
	"Haute-Pyrenees", "Bas-Rhin", "Yonne", "Seine_Maritime", "Orne", "Gard", "Aube", "Aisne", "Lozere", "Paris",
	"Oise", "Tarn", "Essonne", "Aude", "Hauts-de-Seine", "Seine-saint-Denis", "Loiret", "Heraut", "Seine-et-Marne", "Pyrenees-Orientales",
	"Nord", "Pas-de-Calais", "Landes", "Gers", "Manche", "Yvelines", "Indre-et-Loire", "Haute-Gatonne", "Calvados", "Gironde",
	"Mayenne", "Lot-Et-Garonne", "Eure-Et-Loir", "Cantal", "Pyrénées-Atlantiques", "Loir-et-Cher", "Corrèze", "Vendée", "Creuse", "Haute-Vienne",
	"Tarn-et-Garonne", "Ille-et-Vilaine", "Maine-et-Loire", "Aveyron", "Eure", "Lot", "Sarthe", "Loire-Atlantique", "Vienne", "Dordogne",
	"Charente", "Charente-Maritime", "Mayotte", "Haute-Corse", "Corse-du-Sud", "Morbihan", "Finistère", "Côtes-d'Armor", "La Réunion", "Guadeloupe",
	"Martinique"];


var infosCOVID19 = [[460, 1.05, 9.24, 69.1], [363, 1.02, 10.17, 90.9], [339.65, 1.02, 9.74, 94.4], [335.91, 1.02, 10.61, 94.4], [330.87, 1.01, 9.83, 68.6], [306.61, 1.02, 8.97, 94.4], [291.37, 1.07, 11.27, 30.8], [290.13, 1.01, 9.13, 68.6], [289.23, 1.05, 9.3, 71.3], [285.8, 1.05, 7.33, 71.3],
[283.58, 1.01, 7.46, 68.6], [277.32, 0.97, 10.30, 62.8], [270.44, 1.05, 6.24, 71.3], [268.48, 1.01, 7.31, 68.6], [266.54, 1.02, 8.66, 94.4], [264.35, 1.05, 8.47, 71.3], [263.04, 1.02, 8.67, 94.4], [259.34, 0.88, 9.87, 68.3], [257.46, 0.88, 8.38, 68.3], [255.78, 1.02, 7.76, 94.4],
[244.05, 1.01, 6.82, 68.6], [243.41, 1.07, 6.78, 35.9], [237.94, 1.01, 7.69, 68.6], [236.23, 0.88, 10.85, 68.3], [234.83, 1.01, 7.43, 68.6], [234.76, 1.01, 6.88, 68.6], [225.38, 0.88, 9.13, 68.3], [225.25, 0.88, 8.79, 68.3], [222.55, 0.99, 7.68, 57.3], [220.57, 0.88, 7.48, 68.3],
[219.16, 1.06, 6.95, 38.6], [218.38, 0.88, 8.09, 68.3], [217.37, 0.88, 9.72, 68.3], [211.02, 0.98, 7.23, 48.5], [209.77, 0.88, 7.52, 68.3], [209.25, 0.88, 8.65, 68.3], [201.39, 0.98, 8.13, 48.5], [200.79, 0.97, 7.97, 62.8], [199.98, 0.88, 7.68, 68.3], [199.76, 1.05, 6.61, 71.3],
[197.5, 1.06, 6.80, 38.6], [196.54, 1.01, 5.69, 68.6], [195.73, 1.02, 6.22, 94.4], [195.53, 1.08, 6.62, 42.5], [195.38, 1.08, 5.67, 42.5], [193.73, 1.06, 6.53, 38.6], [193.61, 1.01, 6.79, 68.6], [192.76, 0.99, 7.47, 57.3], [192.7, 1.06, 7.64, 38.6], [192.06, 0.98, 4.33, 48.5],
[187.98, 0.99, 7.69, 57.3], [186.39, 1.06, 6.59, 38.6], [183.42, 0.98, 7.30, 48.5], [180.84, 1.06, 5.69, 38.6], [180.76, 0.98, 5.42, 48.5], [179.56, 0.98, 7.42, 48.5], [178.95, 0.97, 6.77, 62.8], [177.19, 1.06, 5.55, 38.6], [174.63, 0.98, 6.96, 48.5], [173.07, 1.06, 5.74, 38.6],
[148.67, 1.04, 6.73, 39.8], [144.7, 1.07, 6.27, 35.9], [143.91, 0.97, 6.18, 62.8], [140.05, 0.88, 4.44, 68.3], [138.33, 1.07, 4.61, 35.9], [136.35, 0.97, 6.36, 62.8], [128.99, 1.07, 5.46, 35.9], [128.22, 1.04, 4.22, 39.8], [126.43, 1.07, 4.51, 35.9], [125.41, 1.07, 5.58, 35.9],
[125.28, 1.06, 5.32, 38.6], [123.74, 1.14, 4.91, 22.8], [117.54, 1.04, 4.69, 39.8], [115.32, 1.06, 4.14, 38.6], [111.87, 1.08, 5.58, 42.5], [111.45, 1.06, 5.40, 38.6], [109.78, 1.04, 4.49, 39.8], [105.35, 1.04, 4.24, 39.8], [101.51, 1.07, 4.90, 35.9], [99.9, 1.07, 4.37, 35.9],
[93.92, 1.07, 4.35, 35.9], [93.34, 1.07, 3.40, 35.9], [88.02, 1.08, 10.82, 16.7], [85.04, 1.43, 2.34, 33.3], [78.81, 1.43, 1.96, 33.3], [70.01, 1.14, 3.43, 22.8], [69.16, 1.14, 2.89, 22.8], [62.23, 1.14, 2.83, 22.8], [31.86, 1.17, 1.99, 11.5], [31.31, 1.28, 3.22, 14.8],
[21.74, 1.29, 1.82, 7.7], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];

var thead_info = ["Departements", "Taux d'incidence", "R0", "Taux de positivité", "Taux d'occupation des lits en réanimation"]
var couleurs = ["orange", "red", "dodgerblue", "indianred"]


var tableau_div = document.getElementById('tableau_div')

var thead = document.createElement('thead')
tableau_div.appendChild(thead)

var tbody = document.createElement('tbody')
tableau_div.appendChild(tbody)


var value_user = document.getElementById("value_user")

var p = document.getElementById('valeur_range_occupation')
p.innerHTML = "Recherche taux au dessus de : " + value_user.value

value_user.addEventListener('change', function () {

	p.innerHTML = "Recherche taux au dessus de : " + value_user.value
	affichageTableau()

})


var value_user_insidence = document.getElementById("value_user_insidence")

var p2 = document.getElementById('valeur_range_incidence')
p2.innerHTML = "Recherche taux au dessus de : " + value_user_insidence.value

value_user_insidence.addEventListener('change', function () {

	p2.innerHTML = "Recherche taux au dessus de : " + value_user_insidence.value
	affichageTableau()

})


function affichageTableau() {

	tableau_div.innerHTML = "";

	var thead = document.createElement('thead')
	tableau_div.appendChild(thead)

	var tbody = document.createElement('tbody')
	tableau_div.appendChild(tbody)

	var tr_head = document.createElement('tr')

	for (i = 0; i < thead_info.length; i++) {

		var th = document.createElement("th")
		th.innerHTML = thead_info[i]
		tr_head.appendChild(th)
		thead.appendChild(tr_head)
	}

	for (i = 0; i < departements.length; i++) {
		
		if (infosCOVID19[i][0] >= value_user_insidence.value) {
			if (infosCOVID19[i][2] >= value_user.value) {
				var tr = document.createElement('tr')
				tbody.appendChild(tr)
				var td = document.createElement("td")
				td.innerHTML = departements[i]
				td.style.color = couleurs[2]
				tr.appendChild(td)
				for (j = 0; j < infosCOVID19[i].length; j++) {
					var td_info = document.createElement("td")
					td_info.innerHTML = infosCOVID19[i][j]
					
					if (j == 0) {
						td_info.style.color = couleurs[3]
					} else if (j == 1) {
						td_info.style.color = couleurs[0]
					} else if (j == 2) {
						if (infosCOVID19[i][j] < 10) {
							td_info.style.color = couleurs[0]
						} else {
							td_info.style.color = couleurs[1]
						}
					} else if (j == 3) {
						if (infosCOVID19[i][j] >= 60) {
							td_info.style.color = couleurs[1]
						} else {
							td_info.style.color = couleurs[0]
						}
					}
					tr.appendChild(td_info)
				}
			}
		}
	}
}

affichageTableau()

var tableau_user_div = document.getElementById('tableau_valeur_user')

select = document.getElementById("departement_select")
select.addEventListener('change', affichage_select)

for(i = 0; i < departements.length; i++){

	var option = document.createElement("option")
	option.innerHTML = departements[i]
	option.value = i
	select.appendChild(option)
}

function affichage_select(){

	tableau_valeur_user.innerHTML = ""

	var tr_head = document.createElement('tr')
	tableau_valeur_user.appendChild(tr_head)

	for (i = 0; i < thead_info.length; i++) {

		var th = document.createElement("th")
		th.innerHTML = thead_info[i]
		tr_head.appendChild(th)
	}

	var tr = document.createElement('tr')
	tableau_valeur_user.appendChild(tr)

	var td_depart = document.createElement("td")
	tr.appendChild(td_depart)
	td_depart.innerHTML = departements[this.value]
	td_depart.style.color = "white"

	for(i = 0; i < infosCOVID19[this.value].length; i++){

		var td = document.createElement("td")
		tr.appendChild(td)
		td.innerHTML = infosCOVID19[this.value][i]
		td.style.color = "white"
	}
}
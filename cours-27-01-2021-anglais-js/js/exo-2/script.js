var anneeEnCour = (new Date()).getFullYear()
var CVHTML = document.getElementById('CV')
var select1 = document.getElementById('annee')
var select2 = document.getElementById('CO2')

var btn = document.getElementById('btn')
btn.addEventListener('click',calculPrix)

for (var i = anneeEnCour-50; i < anneeEnCour+1; i++) {
    var option = document.createElement('option')
    select1.appendChild(option)
    option.innerHTML = i
}

for (var i = 138; i < 213; i++) {
    var option = document.createElement('option')
    select2.appendChild(option)
    option.innerHTML = i
}

function calculPrix() {
    var CV = Number(CVHTML.value)
    var annee = Number(select1.options[select1.selectedIndex].text)
    var age = (new Date()).getFullYear() - annee
    var CO2 = Number(select2.options[select2.selectedIndex].text)
    var stock = 0
    var malus = (CO2/2.66) + ((CO2-138)/2.4)*50
    malus = malus.toFixed(2)

    if ( (CV <= 7) && (CV > 0)) {
        stock = 39 * CV
    } else if ( CV > 7) {
        stock = 39 * CV
    }

    if (age <= 10) {
        // Si age supérieur a 10 prix réduit de moitié
    } else if ((age > 10 ) && ((age < 20))){
        stock /= 2
    // Si age supérieur a 20 montant réduit a 0
    } else {
        stock = 0
    }

    stock = stock.toFixed(2)
    var p = document.createElement('p')
    p = document.body.appendChild(p)
    p.innerHTML = "Le prix du certificat d'immatriculation pour un véhicule de " + CV + " CV est de " + stock + " €";

    var p_malus = document.createElement('p')
    p_malus = document.body.appendChild(p2)
    p_malus.innerHTML = "Le malus écologique s'éléve à " + malus + " €";
}
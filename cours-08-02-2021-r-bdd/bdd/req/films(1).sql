# Database : `films`
# --------------------------------------------------------

#
# Table structure for table `artiste`
#

DROP TABLE IF EXISTS artiste;
CREATE TABLE artiste (
  idActeur int(11) NOT NULL PRIMARY KEY auto_increment,
  nom varchar(30) NOT NULL default '',
  prenom varchar(30) NOT NULL default '',
  anneeNaiss int(11) default NULL
   
) ;

#
# Dumping data for table `artiste`
#

INSERT INTO artiste VALUES (6, 'Cameron', 'James', 1954);
INSERT INTO artiste VALUES (3, 'Hitchcock', 'Alfred', 1899);
INSERT INTO artiste VALUES (4, 'Scott', 'Ridley', 1937);
INSERT INTO artiste VALUES (5, 'Weaver', 'Sigourney', NULL);
INSERT INTO artiste VALUES (8, 'Winslett', 'Kate', NULL);
INSERT INTO artiste VALUES (9, 'Tarkovski', 'Andrei', 1932);
INSERT INTO artiste VALUES (10, 'Woo', 'John', 1946);
INSERT INTO artiste VALUES (11, 'Travolta', 'John', 1954);
INSERT INTO artiste VALUES (12, 'Cage', 'Nicolas', 1964);
INSERT INTO artiste VALUES (13, 'Burton', 'Tim', 1958);
INSERT INTO artiste VALUES (14, 'Depp', 'Johnny', NULL);
INSERT INTO artiste VALUES (15, 'Stewart', 'James', 1908);
INSERT INTO artiste VALUES (16, 'Novak', 'Kim', NULL);
INSERT INTO artiste VALUES (17, 'Mendes', 'Sam', NULL);
INSERT INTO artiste VALUES (18, 'Spacey', 'Kevin', NULL);
INSERT INTO artiste VALUES (19, 'Bening', 'Anette', NULL);
INSERT INTO artiste VALUES (20, 'Eastwood', 'Clint', 1930);
INSERT INTO artiste VALUES (21, 'Hackman', 'Gene', NULL);
INSERT INTO artiste VALUES (22, 'Freeman', 'Morgan', NULL);
INSERT INTO artiste VALUES (23, 'Crowe', 'Russell', NULL);
INSERT INTO artiste VALUES (24, 'Ford', 'Harrison', 1942);
INSERT INTO artiste VALUES (25, 'Hauer', 'Rutger', NULL);
INSERT INTO artiste VALUES (26, 'McTierman', 'John', NULL);
INSERT INTO artiste VALUES (27, 'Willis', 'Bruce', 1955);
INSERT INTO artiste VALUES (28, 'Harlin', 'Renny', NULL);
INSERT INTO artiste VALUES (29, 'Pialat', 'Maurice', 1925);
INSERT INTO artiste VALUES (30, 'Dutronc', 'Jacques', NULL);
INSERT INTO artiste VALUES (31, 'Fincher', 'David', NULL);
INSERT INTO artiste VALUES (32, 'Pitt', 'Brad', NULL);
INSERT INTO artiste VALUES (33, 'Gilliam', 'Terry', NULL);
INSERT INTO artiste VALUES (34, 'Annaud', 'Jean-Jacques', NULL);
INSERT INTO artiste VALUES (35, 'Connery', 'Sean', 1930);
INSERT INTO artiste VALUES (36, 'Slater', 'Christian', NULL);
INSERT INTO artiste VALUES (37, 'Tarantino', 'Quentin', NULL);
INSERT INTO artiste VALUES (38, 'Jackson', 'Samuel L.', NULL);
INSERT INTO artiste VALUES (39, 'Arquette', 'Rosanna', 1959);
INSERT INTO artiste VALUES (40, 'Thurman', 'Uma', NULL);
INSERT INTO artiste VALUES (41, 'Farrelly', 'Bobby', NULL);
INSERT INTO artiste VALUES (42, 'Diaz', 'Cameron', NULL);
INSERT INTO artiste VALUES (43, 'Dillon', 'Mat', NULL);
INSERT INTO artiste VALUES (44, 'Schwartzenegger', 'Arnold', NULL);
INSERT INTO artiste VALUES (45, 'Spielberg', 'Steven', 1946);
INSERT INTO artiste VALUES (46, 'Sheider', 'Roy', NULL);
INSERT INTO artiste VALUES (47, 'Shaw', 'Robert', NULL);
INSERT INTO artiste VALUES (48, 'Dreyfus', 'Richard', NULL);
INSERT INTO artiste VALUES (49, 'Demme', 'Jonathan', 1944);
INSERT INTO artiste VALUES (50, 'Hopkins', 'Anthony', 1937);
INSERT INTO artiste VALUES (51, 'Foster', 'Jodie', 1962);
INSERT INTO artiste VALUES (52, 'Chapman', 'Brenda', NULL);
INSERT INTO artiste VALUES (53, 'Kilmer', 'Val', 1959);
INSERT INTO artiste VALUES (54, 'Fiennes', 'Ralph', 1962);
INSERT INTO artiste VALUES (55, 'Pfeiffer', 'Michelle', 1957);
INSERT INTO artiste VALUES (56, 'Bullock', 'Sandra', 1964);
INSERT INTO artiste VALUES (57, 'Goldblum', 'Jeff', 1952);
INSERT INTO artiste VALUES (58, 'Emmerich', 'Roland', 1955);
INSERT INTO artiste VALUES (59, 'Broderick', 'Matthew', 1962);
INSERT INTO artiste VALUES (60, 'Reno', 'Jean', 1948);
INSERT INTO artiste VALUES (61, 'Wachowski', 'Andy', 1967);
INSERT INTO artiste VALUES (62, 'Reeves', 'Keanu', 1964);
INSERT INTO artiste VALUES (63, 'Fishburne', 'Laurence', 1961);
INSERT INTO artiste VALUES (64, 'De Palma', 'Brian', 1940);
INSERT INTO artiste VALUES (65, 'Cruise', 'Tom', 1962);
INSERT INTO artiste VALUES (66, 'Voight', 'John', 1938);
INSERT INTO artiste VALUES (67, 'BÃ©art', 'Emmanuelle', 1965);
INSERT INTO artiste VALUES (68, 'Kurozawa', 'Akira', 1910);
INSERT INTO artiste VALUES (69, 'Harris', 'Ed', 1950);
INSERT INTO artiste VALUES (70, 'Linney', 'Laura', NULL);
INSERT INTO artiste VALUES (71, 'Girault', 'Jean', NULL);
INSERT INTO artiste VALUES (72, 'De FunÃ¨s', 'Louis', 1914);
INSERT INTO artiste VALUES (73, 'Galabru', 'Michel', 1922);
INSERT INTO artiste VALUES (74, 'Palud', 'HervÃ©', NULL);
INSERT INTO artiste VALUES (75, 'Balasko', 'Josiane', 1950);
INSERT INTO artiste VALUES (76, 'Lavanant', 'Dominique', 1944);
INSERT INTO artiste VALUES (77, 'Lanvin', 'GÃ©rard', 1950);
INSERT INTO artiste VALUES (78, 'Villeret', 'Jacques', 1951);
INSERT INTO artiste VALUES (79, 'Levinson', 'Barry', 1942);
INSERT INTO artiste VALUES (80, 'Hoffman', 'Dustin', 1937);
INSERT INTO artiste VALUES (81, 'Scott', 'Tony', 1944);
INSERT INTO artiste VALUES (82, 'McGillis', 'Kelly', 1957);
INSERT INTO artiste VALUES (83, 'Leconte', 'Patrice', 1947);
INSERT INTO artiste VALUES (84, 'Blanc', 'Michel', 1952);
INSERT INTO artiste VALUES (85, 'Clavier', 'Christian', 1952);
INSERT INTO artiste VALUES (86, 'Lhermite', 'Thierry', 1952);
INSERT INTO artiste VALUES (87, 'Pernnou', 'Marie', NULL);
INSERT INTO artiste VALUES (88, 'Perkins', 'Anthony', 1932);
INSERT INTO artiste VALUES (89, 'Miles', 'Vera', 1929);
INSERT INTO artiste VALUES (90, 'Leigh', 'Janet', 1927);
INSERT INTO artiste VALUES (91, 'Marquand', 'Richard', NULL);
INSERT INTO artiste VALUES (92, 'Hamill', 'Mark', NULL);
INSERT INTO artiste VALUES (93, 'Fisher', 'Carrie', NULL);
INSERT INTO artiste VALUES (94, 'Taylor', 'Rod', NULL);
INSERT INTO artiste VALUES (95, 'Hedren', 'Tippi', 1931);
INSERT INTO artiste VALUES (96, 'Ricci', 'Christina', 1980);
INSERT INTO artiste VALUES (97, 'Walken', 'Christopher', 1943);
INSERT INTO artiste VALUES (98, 'Keitel', 'Harvey', 1939);
INSERT INTO artiste VALUES (99, 'Roth', 'Tim', 1961);
INSERT INTO artiste VALUES (100, 'Penn', 'Chris', 1966);
INSERT INTO artiste VALUES (101, 'Kubrick', 'Stanley', 1928);
INSERT INTO artiste VALUES (102, 'Kidman', 'Nicole', 1967);
INSERT INTO artiste VALUES (103, 'Nicholson', 'Jack', 1937);
INSERT INTO artiste VALUES (104, 'Kelly', 'Grace', 1929);
INSERT INTO artiste VALUES (105, 'Grant', 'Cary', 1904);
INSERT INTO artiste VALUES (106, 'Saint', 'Eva Marie', NULL);
INSERT INTO artiste VALUES (107, 'Mason', 'James', 1909);
INSERT INTO artiste VALUES (110, 'DiCaprio', 'Leonardo', 1974);
INSERT INTO artiste VALUES (109, 'Winslet', 'Kate', 1975);
INSERT INTO artiste VALUES (111, 'Besson', 'Luc', 1959);
INSERT INTO artiste VALUES (112, 'Jovovich', 'Milla', 1975);
INSERT INTO artiste VALUES (113, 'Dunaway', 'Fane', 1941);
INSERT INTO artiste VALUES (114, 'Malkovitch', 'John', 1953);
INSERT INTO artiste VALUES (115, 'Karyo', 'TchÃ©ky', 1953);
INSERT INTO artiste VALUES (116, 'Oldman', 'Gary', 1958);
INSERT INTO artiste VALUES (117, 'Holm', 'Ian', 1931);
INSERT INTO artiste VALUES (118, 'Portman', 'Natalie', NULL);
INSERT INTO artiste VALUES (119, 'Parillaud', 'Anne', 1960);
INSERT INTO artiste VALUES (120, 'Anglade', 'Jean-Hughes', 1955);
INSERT INTO artiste VALUES (121, 'Barr', 'Jean-Marc', 1960);
# --------------------------------------------------------

#
# Table structure for table `film`
#

DROP TABLE IF EXISTS film;
CREATE TABLE film (
  idfilm int(11) NOT NULL PRIMARY KEY auto_increment,
  titre varchar(50) NOT NULL default '',
  annee int(11) NOT NULL default '0',
  idgenre int(11) NOT NULL default '1' references genre(id),
  resume text,
  photo varchar(50) NOT NULL default ''
   
) ;

#
# Dumping data for table `film`
#

INSERT INTO film VALUES (1,'Vertigo', 1958, 2, 'Scottie Ferguson, ancien inspecteur de police, est sujet au vertige depuis qu\'il a vu mourir son\r\n collÃ¨gue. Elster, son ami, le charge de surveiller sa femme, Madeleine, ayant des tendances\r\n suicidaires. Amoureux de la jeune femme Scottie ne remarque pas le piÃ¨ge qui se trame autour\r\n de lui et dont il va Ãªtre la victime... ','vertigo.gif');
INSERT INTO film VALUES (2,'Titanic', 1997, 6, 'Conduite par Brock Lovett, une expÃ©dition amÃ©ricaine fouillant l\'Ã©pave du Titanic remonte Ã  la\r\n surface le croquis d\'une femme nue. AlertÃ©e par les mÃ©dias la dame en question, Rose DeWitt\r\n Bukater, aujourd\'hui centenaire, rejoint les lieux du naufrage, d\'oÃ¹ elle entreprend de conter le\r\n rÃ©cit de son fascinant, Ã©trange et tragique voyage... ','titanic.jpg');
INSERT INTO film VALUES (3,'1492',1996,1,'Evocation de la vie de l\'homme qui decouvrit le continent americain','1492.jpg');
INSERT INTO film VALUES (4,'Volte/Face', 1997, 2, 'Directeur d\'une unitÃ© anti-terroriste, Sean Archer recherche Castor Troy, un criminel responsable de la\r\n mort de son fils six ans plus tÃ´t. Il parvient Ã  l\'arrÃªter mais apprend que Troy a cachÃ© une bombe au Palais\r\n des CongrÃ¨s de Los Angeles. Seul le frÃ¨re de Troy peut la dÃ©samorcer et, pour l\'approcher, Archer se fait\r\n greffer le visage de Troy. ','volteface.jpg');
INSERT INTO film VALUES (5,'Le nom de la rose', 1986, 5, 'En l\'an 1327, dans une abbaye bÃ©nÃ©dictine, le moine franciscain Guillaume de Baskerville,\r\n accompagnÃ© de son jeune novice Adso, enquÃªte sur de mystÃ©rieuses morts qui frappent la\r\n confrÃ©rie. Le secret semble rÃ©sider dans la bibliothÃ¨que, oÃ¹ le vieux Jorge garde jalousement un\r\n livre jugÃ© maudit. ','nomrose.jpg');
INSERT INTO film VALUES (6,'Sleepy Hollow', 1999, 2, 'Nouvelle Angleterre, 1799. A Sleepy Hollow, plusieurs cadavres sont retrouvÃ©s dÃ©capitÃ©s. La\r\n                                      rumeur attribue ces meurtres Ã  un cavalier lui-mÃªme sans tÃªte. Mais le fin limier\r\n                                      new-yorkais Ichabod Crane ne croit pas en ses aberrations. TombÃ© sous le charme de la\r\n                                      vÃ©nÃ©neuse Katrina, il mÃ¨ne son enquÃªte au coeur des sortilÃ¨ges de Sleepy Hollow.. ','sleepy.jpg');
INSERT INTO film VALUES (7,'American Beauty', 1999, 6, 'Lester Burnham, sa femme Carolyn et leur fille Jane mÃ¨nent apparemment une vie des plus\r\n heureuses dans leur belle banlieue. Mais derriÃ¨re cette respectable faÃ§ade se tisse une Ã©trange\r\n et grinÃ§ante tragi-comÃ©die familiale oÃ¹ dÃ©sirs inavouÃ©s, frustrations et violences refoulÃ©es\r\n conduiront inexorablement un homme vers la mort. ','American_Beauty.jpg');
INSERT INTO film VALUES (8,'Impitoyable', 1992, 1, 'LÃ©gendaire hors-la-loi, William Munny s\'est reconverti depuis onze ans en paisible fermier. Il\r\n reprend nÃ©anmoins les armes pour traquer deux tueurs en compagnie de son vieil ami Ned\r\n Logan. Mais ce dernier est capturÃ©, puis Ã©xÃ©cute. L\'honneur et l\'amitiÃ© imposent dÃ¨s lors Ã \r\n Munny de redevenir une derniÃ¨re fois le hÃ©ros qu\'il fut jadis... ','impitoyable.jpg');
INSERT INTO film VALUES (9,'Gladiator', 2000, 1, 'Le gÃ©nÃ©ral romain Maximus est le plus fidÃ¨le\r\n                    soutien de l\'empereur Marc AurÃ¨le, qu\'il a\r\n                    conduit de victoire en victoire avec une\r\n                    bravoure et un dÃ©vouement exemplaires.\r\n                    Jaloux du prestige de Maximus, et plus\r\n                    encore de l\'amour que lui voue l\'empereur,\r\n                    le fils de Marc-AurÃ¨le, Commode, s\'arroge\r\n                    brutalement le pouvoir, puis ordonne\r\n                    l\'arrestation du gÃ©nÃ©ral et son exÃ©cution.\r\n                    Maximus Ã©chappe Ã  ses assassins mais ne peut\r\n                    empÃªcher le massacre de sa famille. CapturÃ©\r\n                    par un marchand d\'esclaves, il devient\r\n                    gladiateur et prÃ©pare sa vengeance.','gladiator.jpg');
INSERT INTO film VALUES (10,'Blade Runner', 1982, 1, 'En 2019, lors de la dÃ©cadence de Los Angeles, des Ãªtres synthÃ©tiques, sans pensÃ©e, sans\r\n Ã©motions, suffisent aux diffÃ©rents travaux d\'entretien. Leur durÃ©e de vie n\'excÃ¨de pas 4 annÃ©es.\r\n Un jour, ces ombres humaines se rÃ©voltent et on charge les tueurs, appelÃ©s Blade Runner, de\r\n les abattre... ','blade.jpg');
INSERT INTO film VALUES (11,'PiÃ¨ge de cristal', 1988, 2, 'John Mc Clane, policier new-yorkais, vient passer Noel a Los Angeles aupres de sa femme.\r\n Dans le building ou elle travaille, il se retrouve temoin de la prise en otage de tout le personnel\r\n par 12 terroristes. Objectif de ces derniers, vider les coffres de la societe. Cache mais isole, il\r\n entreprend de prevenir l\'exterieur...','piege.jpg');
INSERT INTO film VALUES (12,'58 minutes pour vivre', 1990, 2, '\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n Venu attendre sa femme a l\'aÃ©roport, le policier John McLane remarque la prÃ©sence de\r\n terroristes qui ont pris le contrÃ´le des pistes, empÃªchant tout avion d\'atterrir et menaÃ§ant de\r\n laisser les appareils en vol tourner jusqu\'Ã  Ã©puisement de leur kÃ©rosÃ¨ne. John n\'a devant lui que\r\n 58 minutes pour Ã©viter la catastrophe... ','58.jpg');
INSERT INTO film VALUES (13,'Van Gogh', 1990, 1, 'Les derniers jours de la vie de Vincent Van Gogh, rÃ©fugiÃ© Ã  Auvers-sur-Oise, prÃ¨s de chez son\r\n ami et protecteur le docteur Gachet, un ami de son frÃ¨re ThÃ©o. Ce peintre maudit, que les\r\n villageois surnommaient "le fou", n\'avait alors plus que deux mois Ã  vivre, qu\'il passa en\r\n peignant un tableau par jour. ','van.jpg');
INSERT INTO film VALUES (14,'Seven', 1995, 2, 'A New York, un criminel anonyme a dÃ©cidÃ© de commettre 7 meurtres basÃ©s sur les 7 pÃªchÃ©s\r\n capitaux Ã©noncÃ©s dans la Bible : gourmandise, avarice, paresse, orgueil, luxure, envie et colÃ¨re.\r\n Vieux flic blasÃ© Ã  7 jours de la retraite, l\'inspecteur Somerset mÃ¨ne l\'enquÃªte tout en formant\r\n son remplaÃ§ant, l\'ambitieux inspecteur David Mills... ','seven.jpg');
INSERT INTO film VALUES (15,'L\'armÃ©e des douze singes', 1995, 8, 'En 2035, un prisonnier du nom de James Cole est engagÃ© contre son grÃ© comme cobaye dans une expÃ©rience ayant pour but de l\'envoyer dans le passÃ©','singes.jpg');
INSERT INTO film VALUES (16,'Pulp fiction', 1994, 2, 'Pulp Fiction dÃ©crit l\'odyssÃ©e sanglante et burlesque de petits malfrats dans la jungle de\r\n Hollywood, ou s\'entrecroisent les destins de deux petits tueurs, d\'un dangereux gangster mariÃ©\r\n Ã  une camÃ©e, d\'un boxeur roublard, de prÃªteurs sur gages sadiques, d\'un caÃ¯d Ã©lÃ©gant et\r\n dÃ©vouÃ©, d\'un dealer bon mari et de deux tourtereaux Ã  la gachette facile... ','pulp.jpg');
INSERT INTO film VALUES (17,'Mary Ã  tout prix', 1998, 5, 'Pour retrouver l\'amour de sa jeunesse, la belle Mary, Ted-le-looser engage Pat Healy, un\r\n privÃ©. SubjugÃ© par la jeune femme, ce dernier tente de la sÃ©duire en se faisant passer pour un\r\n architecte. Il cache la vÃ©ritÃ© Ã  Ted et fait cause commune avec Tucker, un autre prÃ©tendant,\r\n pour se dÃ©barrasser de l\'encombrant Ted... ','mary.jpg');
INSERT INTO film VALUES (18,'Terminator', 1984, 4, 'Deux creatures venues du futur debarquent sur terre. L\'une d\'entre elles, le Terminator, doit\r\n eliminer une certaine Sarah Connor, qui doit enfanter celui qui sera le chef d\'un groupe de\r\n resistants. L\'autre, Kyle Reese, est charge par les rebelles de defendre Sarah... ','terminator.jpg');
INSERT INTO film VALUES (19,'Les dents de la mer', 1975, 4, 'Dans la station balnÃ©aire d\'Amityville, un requin gÃ©ant frappe Ã  plusieurs reprises. Soucieux\r\n d\'une bonne saison touristique, le maire interdit au sherif Brody de fermer les plages. Une prime\r\n est offerte et le cÃ©lÃ¨bre chasseur de requin Quint se lance avec Brody et l\'ichtyologue Hooper Ã \r\n la poursuite du monstre... ','dent.jpg');
INSERT INTO film VALUES (20,'Le prince d\'Egypte', 1998, 3, 'A travers l\'histoire de deux frÃ¨res, tous deux princes du plus grand empire sur terre, Ã©vocation de l\'Ã©popÃ©e de MoÃ¯se','prince.jpg');
INSERT INTO film VALUES (21,'Godzilla', 1998, 4, 'Issu des radiations atomiques engendrÃ©es par les essais nuclÃ©aires en PolynÃ©sie, un monstre\r\n gigantesque, aussi haut qu\'un building, se dirige vers New York, semant le chaos sur son\r\n passage. Pour tenter de stopper cette crÃ©ature destructrice, l\'armÃ©e s\'associe Ã  une Ã©quipe de\r\n scientifiques amÃ©ricains et Ã  un Ã©nigmatique enquÃªteur franÃ§ais... ','godzilla.jpg');
INSERT INTO film VALUES (22,'Matrix', 1999, 4, 'Dans un monde oÃ¹ tout ce qui semble rÃ©el est en fait Ã©laborÃ© par l\'univers Ã©lectronique baptisÃ©\r\n la Matrice, NÃ©o, un programmeur, est contactÃ© par un certain Morpheus. D\'aprÃ¨s lui, NÃ©o\r\n serait le LibÃ©rateur tant attendu, le seul capable de mettre en Ã©chec l\'omnipotence de la\r\n Matrice et rendre ses droits Ã  la rÃ©alitÃ©... ','matrix.jpg');
INSERT INTO film VALUES (23,'Mission: Impossible', 1996, 4, 'ChargÃ© d\'une nouvelle mission, l\'agent du contre espionnage Ethan Hunt tombe avec son\r\n Ã©quipe dans un piÃ¨ge sanglant. Seul survivant avec Claire, la jeune Ã©pouse de son regrettÃ© chef\r\n Jim Phelps, Ethan se retrouve accusÃ© de trahison. En fuite, il prÃ©pare sa contre-attaque,\r\n recrutant l\'homme de main Krieger et le pirate informatique Luther... ','mission.jpg');
INSERT INTO film VALUES (24,'Kagemusha', 1980, 1, 'Au XVIe siecle, Takeda, grand seigneur trouble par les guerres civiles de son pays, fait appel a\r\n un Kagemusha pour l\'aider dans ses batailles. Takeda est blesse et avant de mourir, il exige que\r\n sa mort soit tenue secrete pendant trois ans afin d\'eviter un eclatement du clan. Le Kagemusha\r\n devra le remplacer... ','kage.jpg');
INSERT INTO film VALUES (25,'Les pleins pouvoirs', 1997, 2, 'Luther Whitney est l\'as des cambrioleurs. OccupÃ© Ã  vider le coffre de l\'influent Walter Sullivan,\r\n il est tÃ©moin d\'un meurtre sadique impliquant le PrÃ©sident des Etats-Unis et les services\r\n secrets. SoupÃ§onnÃ© par la police d\'en Ãªtre l\'auteur, il se retrouve Ã©galement traquÃ© par les\r\n tueurs, qui ont compris qu\'ils ont Ã©tÃ© observÃ©s.','pouvoir.jpg');
INSERT INTO film VALUES (26,'Le gendarme et les extra-terrestres', 1978, 5, 'En anglais: Gendarme and the Creatures from Outer Space !\r\n','gendarme.jpg');
INSERT INTO film VALUES (27,'Les frÃ¨res pÃ©tards', 1986, 5, 'FauchÃ©s, Momo et Manu acceptent de convoyer des statuettes depuis Amsterdam, et dÃ©couvrent qu\'elles contiennent de la drogue','frere.jpg');
INSERT INTO film VALUES (28,'Le monde perdu', 1997, 4, 'Quatre ans aprÃ¨s le terrible fiasco de son Jurassic Park, le milliardaire John Hammond rappelle le Dr Ian Malcolm pour l\'informer de son nouveau projet','monde.jpg');
INSERT INTO film VALUES (29,'Rain Man', 1988, 6, 'A la mort de son pÃ¨re, Charlie se voit dÃ©possÃ©der de son hÃ©ritage par un frÃ¨re dont il ignorait\r\n l\'existence, Raymond. Celui-ci est autiste et vit dans un hÃ´pital psychiatrique. Charlie enlÃ¨ve\r\n Raymond afin de prouver qu\'il est capable de s\'en occuper et de toucher l\'hÃ©ritage. ','rain.jpg');
INSERT INTO film VALUES (30,'Top Gun', 1986, 1, '\r\n Pilote de chasse Ã©mÃ©rite mais casse-cou Maverick Mitchell est admis Ã  Top Gun, l\'Ã©cole de\r\n l\'Ã©lite de l\'aÃ©ronavale. Son manque de prudence lui attire les foudres de ses supÃ©rieurs et la\r\n haine de certains coÃ©quipiers. Il perd subitement la foi et confiance en lui quand son ami de\r\n toujours meurt en vol et qu\'il s\'en croit responsable... ','top.jpg');
INSERT INTO film VALUES (31,'Les bronzÃ©s font du ski', 1979, 5, 'Apres avoir passe des vacances d\'ete ensemble, Bernard, Nathalie, Gigi, Jerome, Popeye,\r\n Jean-Claude et Christiane se retrouvent aux sports d\'hiver. Tous ont leurs problemes de coeur\r\n ou d\'argent, mais il faut bien vivre avec. Avant de se separer, se perdre dans la montagne leur\r\n permet de gouter aux joies de la "vraie vie" paysanne... ','bronzes.jpg');
INSERT INTO film VALUES (32,'MICROCOSMOS', 1996, 3, ' \r\n\r\nUne heure quinze sur une planÃ¨te inconnue : la Terre, redÃ©couverte Ã  l\'echelle du centimÃ¨tre. Ses habitants,\r\n des crÃ©atures fantastiques : les insectes et autres animaux de l\'herbe et de l\'eau. Ses paysages : les forÃªts\r\n impenÃ©trables que sont les touffes d\'herbe, ou des gouttes de rosÃ©e grosses comme des ballons... ','micro.jpg');
INSERT INTO film VALUES (33,'Psychose', 1960, 6, 'AprÃ¨s avoir volÃ© 40 000 dollars, Marion Crane se rend dans un motel tenu par Norman Bates. Elle est\r\n poignardÃ©e sous sa douche par une femme. Norman fait disparaitre le corps et les affaires de la jeune\r\n femme. Mais Sam, le fiancÃ© de Marion, inquiet de ne pas avoir de nouvelles, engage un dÃ©tective pour la\r\n retrouver... ','psy.jpg');
INSERT INTO film VALUES (34,'Le retour du Jedi', 1983, 4, 'Luke Skywalker s\'introduit chez Jabba pour delivrer Han Solo et la princesse Leia, tandis que l\'Empire\r\n reconstruit une deuxieme "Etoile de la Mort". Luke se rend ensuite au chevet de Yoda qui est mourant. Il lui\r\n apprend que Leia est sa soeur. Luke forme un commando pour attaquer l\'Etoile, tandis qu\'il affronte son\r\n pere, Darth Vador... ','retour.jpg');
INSERT INTO film VALUES (35,'Les oiseaux', 1963, 1, 'Melanie Daniels se rend Ã  Bodega Bay pour offrir deux perruches en cage Ã  Cathy, la soeur de l\'avocat\r\n Mitch Brenner. AttaquÃ©e par une mouette, Melanie reste chez les Brenner pour la nuit. Mais d\'autres\r\n Ã©vÃ¨nements Ã©tranges se produisent: des enfants sont blessÃ©s par des corbeaux et la maison de Mitch est\r\n envahie par des milliers d\'oiseaux. ','oiseau.jpg');
INSERT INTO film VALUES (36,'Reservoir dogs', 1992, 2, '\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n Voleurs de profession, Joe Cabot et son fils Eddie engagent un groupe de six criminels pour le cambriolage\r\n d\'un diamantaire. MalgrÃ© toutes les prÃ©cautions prises, la police est sur place le jour J, et l\'opÃ©ration se\r\n solde par un massacre. Les survivants du gang se rÃ©unissent pour rÃ¨gler leurs comptes, chacun\r\n soupÃ§onnant l\'autre d\'avoir trahi. ','dog.jpg');
INSERT INTO film VALUES (37,'Eyes Wide Shut', 1999, 6, 'Un couple de bourgeois, cÃ©dant Ã  la jalousie et Ã  l\'obsession sexuelle, entreprend un voyage psychologique\r\n Ã  la recherche de son identitÃ©. Le mari, au bout de son pÃ©riple nocturne, revenu de ses dÃ©sirs, ne trouvera\r\n finalement auprÃ¨s de son Ã©pouse qu\'un compromis banal mais complice, les yeux ouverts Ã  tout jamais sur\r\n un rÃªve impossible. ','eyes.jpg');
INSERT INTO film VALUES (38,'Shining', 1980, 6, 'Jack Torrance s\'installe avec sa femme et son fils Danny dans un hÃ´tel fermÃ© Ã  la morte saison afin d\'Ã©crire\r\n un roman. Il apprend que le gardien prÃ©cÃ©dent a tuÃ© sa femme et ses deux filles avant de se suicider. TrÃ¨s\r\n vite, Jack va s\'apercevoir que des choses Ã©tranges se passent autour de lui et que son fils a des pouvoirs\r\n extrasensoriels... ','shining.jpg');
INSERT INTO film VALUES (39,'Pas de printemps pour Marnie', 1964, 5, 'Marnie est engagÃ©e comme secrÃ©taire chez un editeur, Mark Rutland. Celui-ci amoureux d\'elle, dÃ©couvre\r\n qu\'elle est kleptomane et l\'oblige Ã  l\'Ã©pouser en la menaÃ§ant de la dÃ©noncer. En s\'apercevant que Marnie a\r\n la phobie de la cou leur rouge, Mark tente de remonter dans le passÃ© de la jeune femme afin de comprendre\r\n les raisons de sa nÃ©vrose. ','marnie.jpg');
INSERT INTO film VALUES (40,'FenÃªtre sur cour', 1954, 1, 'En repos forcÃ© Ã  cause d\'une jambe plÃ¢trÃ©e, le reporter L.B. Jefferies observe au tÃ©lÃ©objectif les voisins de\r\n l\'immeuble d\'en face. C\'est ainsi qu\'il remarque le curieux manÃ¨ge d\'un reprÃ©sentant de commerce, M.\r\n Thorwald, qu\'il soupconne trÃ¨s vite d\'avoir assassinÃ© sa femme... ','fenetre.jpg');
INSERT INTO film VALUES (41,'La mort aux trousses', 1959, 2, 'Roger Thornhill, publiciste, est pris dans le hall de son hÃ´tel pour un certain Kaplan, un espion. Deux\r\n hommes tentent de le tuer et quand il retrouve l\'un de ses agresseurs, celui-ci est assassinÃ© devant ses\r\n yeux. Pris pour un meurtrier, il est obligÃ© de fuir vers Chicago... ','mort.jpg');
INSERT INTO film VALUES (42,'Jeanne d\'Arc', 1999, 1, 'L\'Ã©popee de Jeanne qui assista, petite fille, au pillage de son village Domremy par l\'armÃ©e anglaise','jeanne.jpg');
INSERT INTO film VALUES (43,'Le cinquiÃ¨me Ã©lÃ©ment', 1997, 4, 'Au XXIIIÃ¨me siÃ¨cle, dans un univers Ã©trange et colorÃ©, oÃ¹ tout espoir de survie est impossible sans la\r\n dÃ©couverte du CinquiÃ¨me ElÃ©ment, un hÃ©ros peu ordinaire affronte le mal pour sauver l\'humanitÃ©. ','5.jpg');
INSERT INTO film VALUES (44,'LÃ©on', 1994, 2, '"LÃ©on est un tueur. Un de la pire espÃ¨ce. Il est introuvable, indÃ©tectable, pire qu\'un sous-marin. Son ombre\r\n est comme une menace de danger permanent sur New York. Indestructible LÃ©on ? Oui, jusqu\'Ã  ce qu\'une\r\n petite souris pÃ©nÃ¨tre son univers. Une toute petite souris aux yeux immenses..." ','leon.jpg');
INSERT INTO film VALUES (45,'Nikita', 1990, 2, 'Nikita, droguÃ©e et violente, est prise en mains par des psychiatres qui la rÃ©Ã©duquent, la conditionnent, afin\r\n d\'en faire une tueuse Ã  la botte des Services Secrets. Plus tard, rÃ©alisant ce qu\'elle est devenue, un pion\r\n sans vie privÃ©e que l\'on manipule, elle tente d\'Ã©chapper Ã  ses commanditaires. ','nikita.jpg');
INSERT INTO film VALUES (46,'Le grand bleu', 1988, 1, 'Jacques Mayol et Enzo Molinari se connaissent depuis l\'enfance. Tous deux experts en apnÃ©e, s\'affrontent\r\n continuellement pour obtenir le record du monde de plongÃ©e. Toujours en rivalitÃ©, les deux hommes\r\n descendent de plus en plus profond, au risque de leurs vies. Le film est ressorti en janvier 89 en version\r\n longue d\'une durÃ©e de 2h40. ','bleu.jpg');
INSERT INTO film VALUES (47,'Alien', 1979, 4, 'PrÃ¨s d\'un vaisseau spatial Ã©chouÃ© sur une lointaine planÃ¨te, des Terriens en mission dÃ©couvrent\r\n de bien Ã©tranges "oeufs". Ils en ramÃ¨nent un Ã  bord, ignorant qu\'ils viennent d\'introduire parmi\r\n eux un huitiÃ¨me passager particuliÃ¨rement fÃ©roce et meurtrier. ','alien.jpg');
INSERT INTO film VALUES (48,'Le silence des agneaux', 1990, 6, 'Afin de retrouver la piste d\'un tueur surnommÃ© Buffalo Bill car il scalpe les femmes qu\'il\r\n assassine, la jeune stagiaire du FBI Clarice Starling est dÃ©pÃªchÃ©e auprÃ¨s d\'Hannibal Lecter,\r\n prisonnier pour avoir dÃ©vorÃ© ses victimes. La coopÃ©ration de ce dernier devrait permettre Ã \r\n Clarice de saisir et d\'anticiper le comportement de Buffalo... ','silence.jpg');
# --------------------------------------------------------

#
# Table structure for table `genre`
#

DROP TABLE IF EXISTS genre;
CREATE TABLE genre (
  id int(11) NOT NULL PRIMARY KEY auto_increment,
  nomgenre varchar(100) NOT NULL default ''
  
) ;

#
# Dumping data for table `genre`
#

INSERT INTO genre VALUES (1, 'Histoire');
INSERT INTO genre VALUES (2, 'Policier');
INSERT INTO genre VALUES (3, 'Animation');
INSERT INTO genre VALUES (4, 'Fantastique');
INSERT INTO genre VALUES (5, 'Comedie');
INSERT INTO genre VALUES (6, 'Drame');

# --------------------------------------------------------

#
# Table structure for table `role`
#

DROP TABLE IF EXISTS role;
CREATE TABLE role (
  idfilm int(11) default '0' references film(idfilm),
  idActeur int(11) NOT NULL default '0' references artiste(idActeur) ,
  nomrole varchar(30) default NULL ,
  PRIMARY KEY  (idfilm,idActeur)

) ;

#
# Dumping data for table `role`
#

INSERT INTO role VALUES (7, 19, 'Carolyn Burnham');
INSERT INTO role VALUES (7, 18, 'Lester Burnham');
INSERT INTO role VALUES (47, 5, 'Ripley');
INSERT INTO role VALUES (2, 110, 'Jack Dawson');
INSERT INTO role VALUES (4, 11, 'Sean Archer/Castor Troy');
INSERT INTO role VALUES (6, 14, 'Constable Ichabod Crane');
INSERT INTO role VALUES (1, 15, 'John Ferguson');
INSERT INTO role VALUES (1, 16, 'Madeleine Elster');
INSERT INTO role VALUES (8, 20, 'William Munny');
INSERT INTO role VALUES (8, 21, 'Little Bill Dagget');
INSERT INTO role VALUES (8, 22, 'Ned Logan');
INSERT INTO role VALUES (9, 23, 'Maximus');
INSERT INTO role VALUES (10, 24, 'Deckard');
INSERT INTO role VALUES (10, 25, 'Batty');
INSERT INTO role VALUES (11, 27, 'McLane');
INSERT INTO role VALUES (12, 27, 'McLane');
INSERT INTO role VALUES (13, 30, 'Van Gogh');
INSERT INTO role VALUES (14, 32, 'Mills');
INSERT INTO role VALUES (14, 22, 'Somerset');
INSERT INTO role VALUES (14, 18, 'Doe');
INSERT INTO role VALUES (15, 27, 'Cole');
INSERT INTO role VALUES (5, 35, 'Baskerville');
INSERT INTO role VALUES (5, 36, 'de Melk');
INSERT INTO role VALUES (16, 11, 'Vincent Vega');
INSERT INTO role VALUES (16, 38, 'Jules Winnfield');
INSERT INTO role VALUES (16, 39, 'Jody');
INSERT INTO role VALUES (16, 27, 'Butch Coolidge');
INSERT INTO role VALUES (16, 40, 'Mia Wallace');
INSERT INTO role VALUES (17, 42, 'Mary Jensen Matthews');
INSERT INTO role VALUES (17, 43, 'Pat Healy');
INSERT INTO role VALUES (18 ,44, 'Terminator');
INSERT INTO role VALUES (19, 46, 'Martin Brody');
INSERT INTO role VALUES (19, 47, 'Quint');
INSERT INTO role VALUES (19, 48, 'Matt Hooper');
INSERT INTO role VALUES (48, 50, 'Dr. Hannibal Lecter');
INSERT INTO role VALUES (48, 51, 'Clarice Starling');
INSERT INTO role VALUES (20, 53, 'Moise');
INSERT INTO role VALUES (20, 54, 'Ramses');
INSERT INTO role VALUES (20, 55, '\n');
INSERT INTO role VALUES (20, 56, 'Miriam');
INSERT INTO role VALUES (20, 57, 'Aaron');
INSERT INTO role VALUES (21, 59, 'Dr. Nikos Tatopoulos');
INSERT INTO role VALUES (21, 60, 'Philippe RoachÃ©');
INSERT INTO role VALUES (22, 62, 'Neo');
INSERT INTO role VALUES (22, 63, 'Morpheus');
INSERT INTO role VALUES (23, 65, 'Ethan Hunt');
INSERT INTO role VALUES (23, 66, 'Jim Phelps');
INSERT INTO role VALUES (23, 67, 'Claire Phelps');
INSERT INTO role VALUES (23, 60, 'Franz Krieger');
INSERT INTO role VALUES (25, 20, 'Luther Whitney');
INSERT INTO role VALUES (25, 21, 'Le prÃ©sident Richmond');
INSERT INTO role VALUES (25, 69, 'Seth Frank');
INSERT INTO role VALUES (26, 72, 'Inspecteur Cruchot');
INSERT INTO role VALUES (26, 73, 'Adjudant Gerber');
INSERT INTO role VALUES (27, 75, 'Aline');
INSERT INTO role VALUES (27, 73, 'Momo\'s Father');
INSERT INTO role VALUES (27, 76, 'Une policiÃ¨re');
INSERT INTO role VALUES (27, 77, 'Manu');
INSERT INTO role VALUES (27, 78, 'Momo');
INSERT INTO role VALUES (28, 57, 'Dr. Ian Malcolm');
INSERT INTO role VALUES (29, 80, 'Raymond Babbitt');
INSERT INTO role VALUES (29, 65, 'Charlie Babbitt');
INSERT INTO role VALUES (30, 65, 'Lt. Pete \'Maverick\' Mitchell');
INSERT INTO role VALUES (30, 82, 'Charlotte Blackwood');
INSERT INTO role VALUES (30, 53, 'Iceman');
INSERT INTO role VALUES (31, 75, 'Nathalie Morin');
INSERT INTO role VALUES (31, 84, 'Jean-Claude Dus');
INSERT INTO role VALUES (31, 85, 'JÃ©rÃ´me');
INSERT INTO role VALUES (31, 76, 'Christiane');
INSERT INTO role VALUES (31, 86, 'Popeye');
INSERT INTO role VALUES (33, 88, 'Norman Bates');
INSERT INTO role VALUES (33, 89, 'Lila Crane');
INSERT INTO role VALUES (33, 90, 'Marion Crane');
INSERT INTO role VALUES (34, 92, 'Luke Skywalker');
INSERT INTO role VALUES (34, 24, 'Han Solo');
INSERT INTO role VALUES (34, 93, 'Princesse Leia');
INSERT INTO role VALUES (35, 94, 'Mitch Brenner');
INSERT INTO role VALUES (35, 95, 'Melanie Daniels');
INSERT INTO role VALUES (6, 96, 'Katrina Anne Van Tassel');
INSERT INTO role VALUES (6, 97, 'Le cavalier');
INSERT INTO role VALUES (36, 98, 'Mr. White/Larry');
INSERT INTO role VALUES (36, 99, 'Freddy Newendyke/Mr. Orange');
INSERT INTO role VALUES (36, 100, 'Nice Guy Eddie');
INSERT INTO role VALUES (36, 37, 'Mr. Brown');
INSERT INTO role VALUES (4, 12, 'Castor Troy/Sean Archer');
INSERT INTO role VALUES (37, 102, 'Alice Harford');
INSERT INTO role VALUES (38, 103, 'Jack Torrance');
INSERT INTO role VALUES (39, 95, 'Marnie Edgar');
INSERT INTO role VALUES (39, 35, 'Mark R');
INSERT INTO role VALUES (41, 105, 'Roger O. Thornhill');
INSERT INTO role VALUES (41, 106, 'Eve Kendall');
INSERT INTO role VALUES (41, 107, 'Philipp Vandamm');
INSERT INTO role VALUES (2, 109, 'Rose DeWitt Bukater');
INSERT INTO role VALUES (42, 112, 'Jeanne d\'Arc');
INSERT INTO role VALUES (42, 113, 'Yolande d\'Aragon');
INSERT INTO role VALUES (42, 114, 'Charles VII');
INSERT INTO role VALUES (42, 115, 'Dunois');
INSERT INTO role VALUES (43, 27, 'Major Korben Dalla');
INSERT INTO role VALUES (43, 116, 'Jean-Baptiste Emmanuel Zorg');
INSERT INTO role VALUES (43, 112, 'Leeloo');
INSERT INTO role VALUES (43, 117, 'Vito Cornelius');
INSERT INTO role VALUES (44, 60, 'LÃ©on');
INSERT INTO role VALUES (44, 116, 'Norman Stansfield');
INSERT INTO role VALUES (44, 118, 'Mathilda');
INSERT INTO role VALUES (45, 119, 'Nikita');
INSERT INTO role VALUES (45, 115, 'Bob');
INSERT INTO role VALUES (45, 120, 'Marco');
INSERT INTO role VALUES (46, 39, 'Johanna');
INSERT INTO role VALUES (46, 121, 'Jacques Mayol');
INSERT INTO role VALUES (46, 60, 'Enzo Molinari');
# --------------------------------------------------------

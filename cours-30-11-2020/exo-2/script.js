var vent = 5
var temp = -6

var stock

for(vent; vent <= 110; vent+=5){

    document.write("<br />" + vent + "km/h ")

    for(var i=-6; i <=10; i+=2){
        stock = 13.12 + (0.6215 * i) + (0.3965 * i - 11.37) * Math.pow(vent, 0.16)
        document.write(stock.toFixed(2) + "///")
    }

}
/*

function convertFahrenheit2Celsius( valueF, language )
{
  if ( valueF != "" )
  {
    var degF, degC;

    degF = parseInt(valueF.replace(/\,/, '.'), 10);
    if ( degF > 50 )
    {
      if (language == "fr")
        alert("La temp\xE9rature doit \xEAtre inf\xE9rieure ou \xE9gale \xE0 50\xB0F ou 10\xB0C.");
      else
        alert("You must enter a temperature under 50\xB0F or 10\xB0C.");
      degF = 50;
      document.WindchillCalcform.AirDegreesF.value = degF.toFixed(0);
    }
    degC = 100.0 * (degF - 32.0) / (212.0 - 32.0);

    document.WindchillCalcform.AirDegreesC.value = degC.toFixed(0);
  }
}

function convertCelsius2Fahrenheit( valueC, language )
{
  if ( valueC != "" )
  {
    var degC, degF;

    degC = parseInt(valueC.replace(/\,/, '.'), 10);
    if ( degC > 10 )
    {
      if (language == "fr")
        alert("La temp\xE9rature doit \xEAtre inf\xE9rieure ou \xE9gale \xE0 10\xB0C.");
      else
        alert("You must enter a temperature under 10\xB0C or 50\xB0F.");
      degC = 10;
      document.WindchillCalcform.AirDegreesC.value = degC.toFixed(0);
    }
    degF = ((212.0 - 32.0) * degC / 100.0) + 32.0;

    document.WindchillCalcform.AirDegreesF.value = degF.toFixed(0);
  }
}

function convertMPH2KPH( language )
{
  var mph, kph, tmp;

  tmp = document.WindchillCalcform.WindSpeedMPH.value;
  if (language == "fr")
    tmp = tmp.replace(/\,/, '.');
  mph = Math.abs(parseInt(tmp, 10));
  if ( mph > 62.143 )
  {
    if (language == "fr")
      alert("La vitesse maximale du vent doit \xEAtre inf\xE9rieure ou \xE9gale \xE0 62 mi/h ou 100 km/h.");
    else
      alert("You must enter a wind speed under 62 mph or 100 kph.");
    mph = 62.143;
  }
  tmp = mph.toFixed(3);
  if (language == "fr")
    tmp = tmp.replace(/\./, ',');
  document.WindchillCalcform.WindSpeedMPH.value = tmp;
  
  kph = 1.6092 * mph;

  document.WindchillCalcform.WindSpeedKPH.value = kph.toFixed(3);
}

function convertKPH2MPH( language )
{
  var kph, mph, tmp;

  tmp = document.WindchillCalcform.WindSpeedKPH.value;
  if (language == "fr")
    tmp = tmp.replace(/\,/, '.');
  kph = Math.abs(parseInt(tmp, 10));
  if ( kph > 100 )
  {
    if (language == "fr")
      alert("La vitesse maximale du vent doit \xEAtre inf\xE9rieure ou \xE9gale \xE0 100 km/h.");
    else
      alert("You must enter a wind speed under 100 kph or 62 mph.");
    kph = 100;
  }
  tmp = kph.toFixed(3);
  if (language == "fr")
    tmp = tmp.replace(/\./, ',');
  document.WindchillCalcform.WindSpeedKPH.value = tmp;
  mph = kph / 1.6092;

  document.WindchillCalcform.WindSpeedMPH.value = mph.toFixed(3);
}

function CalculateWindchill( language )
{
  var windSpeedMPH, airTempF, windchillF, nWindchillF, windchillC, nWindchillC, tmp;

    // Make sure that we have something to parse from the form
  if ( language == "en" )
  {
    if ( document.WindchillCalcform.AirDegreesF.value == "" )
      document.WindchillCalcform.AirDegreesF.value = "32";
    if ( document.WindchillCalcform.AirDegreesC.value == "" )
      document.WindchillCalcform.AirDegreesC.value = "0";
    if ( document.WindchillCalcform.WindSpeedMPH.value == "" )
      document.WindchillCalcform.WindSpeedMPH.value = "0";
    if ( document.WindchillCalcform.WindSpeedKPH.value == "" )
      document.WindchillCalcform.WindSpeedKPH.value = "0";
  }
  else
  {
    if ( document.WindchillCalcform.AirDegreesF.value == "" )
      document.WindchillCalcform.AirDegreesF.value = "32";
    if ( document.WindchillCalcform.AirDegreesC.value == "" )
      document.WindchillCalcform.AirDegreesC.value = "0";
    if ( document.WindchillCalcform.WindSpeedMPH.value == "" )
      document.WindchillCalcform.WindSpeedMPH.value = "0";
    if ( document.WindchillCalcform.WindSpeedKPH.value == "" )
      document.WindchillCalcform.WindSpeedKPH.value = "0";
  }

  tmp = document.WindchillCalcform.AirDegreesF.value;
  if (language == "fr")
    tmp = tmp.replace(/\,/, '.');
  airTempF = parseInt(tmp, 10);
  tmp = document.WindchillCalcform.WindSpeedMPH.value;
  if (language == "fr")
    tmp = tmp.replace(/\,/, '.');
  windSpeedMPH = parseInt(tmp, 10);

  if ( windSpeedMPH > 0 )
  {
    // Old formula
    windchillF = (0.0817 * (3.71 * (Math.pow(windSpeedMPH, 0.5)) + 5.81 - 0.25 * windSpeedMPH) * (airTempF - 91.4) + 91.4);
    // New formula
    nWindchillF = 35.74 + (0.6215 * airTempF) - (35.75 * Math.pow(windSpeedMPH, 0.16)) + (0.4275 * airTempF * Math.pow(windSpeedMPH, 0.16));
  }
  else
  {
    windchillF = airTempF;
    nWindchillF = airTempF;
  }

  document.WindchillCalcform.WindchillF.value = windchillF.toFixed(0);
  windchillC = 100.0 * (windchillF - 32.0) / (212.0 - 32.0);
  document.WindchillCalcform.WindchillC.value = windchillC.toFixed(0);
  document.WindchillCalcform.NWindchillF.value = nWindchillF.toFixed(0);
  nWindchillC = 100.0 * (nWindchillF - 32.0) / (212.0 - 32.0);
  document.WindchillCalcform.NWindchillC.value = nWindchillC.toFixed(0);
}
*/
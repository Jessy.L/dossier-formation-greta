const EMAIL="test@test.com";
const MDPMEM="123456";

var cpt=0;
var essai=3;

var input_email = document.getElementById('inputEmail')
var input_mdp = document.getElementById('inputPassword')

var errorMdp = document.getElementById('errorMdp')
var errorEmail = document.getElementById('errorEmail')

errorEmail.style.display = "none";
errorMdp.style.display = "none";


(function() {
    'use strict';
  
      //  AJOUT D'UN EVENT LISTENER SUR L'ACTION 'load' D'UN DOCUMENT :
      window.addEventListener('load', function() {

      // Récupération des formulaires auxquels appliquer des styles de validation Bootstrap personnalisés

      var forms = document.getElementsByClassName('needs-validation');

      // Loop over them and prevent submission

      var validation = Array.prototype.filter.call(forms, function(form) {
        console.log('test');

        //  AJOUT D'UN EVENT LISTENER SUR L'ACTION 'SUBMIT' 
        form.addEventListener('submit', function(event) {
          
          //  SI FORM VALIDATION KO
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          
          //  SI FORM VALIDATION OK
  
          /*************   TON CODE   *************/
          if(EssaiMdp() === false)
          {
            
            event.preventDefault();
            event.stopPropagation();
          }
          /*************   TON CODE   *************/
          console.log(cpt +' '+ essai)
  
          if(parseInt(cpt) > parseInt(essai) ){
            var button = document.getElementById('valid-button')
            button.style.pointerEvents  = "none";
            button.cursor =  "default";
            decompte()
          }

  
        }, false);
      });
    }, false);
  })();



function EssaiMdp() { 

    
    var mdp = document.getElementById("inputPassword").value; 
    var log = document.getElementById("inputEmail").value; 
    

    
    errorEmail.style.display = "none";
    errorMdp.style.display = "none";
    input_email.style.borderColor = "#495057"
    input_mdp.style.borderColor = "#495057"
    
    if(log == "" || mdp == ""){

        if(log == ""){
            errorEmail.style.display = "block";
            errorEmail.style.color = "red"
            input_email.style.borderColor = "red"
        }

        if(mdp == ""){
            errorMdp.style.display = "block";
            errorMdp.style.color = "red"
            input_mdp.style.borderColor = "red"
        }
        return false;

    }else{
        
        if(mdp === MDPMEM && log === EMAIL) {

            errorEmail.style.display = "none";
            errorMdp.style.display = "none";

            form.addClass('was-validated'); 
            cpt = 0; 
            return true; 
        } else { 
            if(parseInt(cpt) <= parseInt(essai)) { 
                document.getElementById("errorMsg").style.display = ""; 
                document.getElementById("errorMsg").innerHTML = "Mot de passe incorrect, il vous reste " + (parseInt(essai)-parseInt(cpt)) + " essai(s)"; 
                cpt ++; 
            } else { 
                document.getElementById("errorMsg").style.display = "none"; 
            } 
            return false;
        }
    }

}

function decompte() { 
    
    var button = document.getElementById('valid-button')
        button.style.pointerEvents  = "none";
        button.cursor =  "default";

    var compte_texte = document.getElementById('chrono')
    var count = 10


    setInterval(function(){ 
        count = count - 1;
        compte_texte.innerHTML = "Trop de tentavite veuillez attendre " + count + " seconde(s)."
        if(count == 0){
            window.location.reload();
        }
    },1000)
} 
Programme net_salaire ;

VAR nombre1 : entier = 10 ;
	nombre2 : entier = 0 ;
	nombre3 : entier = 0 ;


DEBUTPROG
	Afficher("Tapez 2 nombre : ", CRLF);
	Saisir(nombre2,nombre3);
	
	SI(nombre1 > nombre2) ALORS 
		Afficher("Le nombre : ", nombre1, " > ", nombre2, CRLF );
	FINSI
	
	SI (nombre1 < nombre3) ALORS
		Afficher("LE nombre 1 ", nombre1, " < ", nombre3, CRLF);
	FINSI

	SI (nombre1 < nombre3) ET (nombre1 > nombre3) ALORS
		Afficher(CRLF ,"TOUT EST VRAI ", CRLF);
	FINSI

FINPROG

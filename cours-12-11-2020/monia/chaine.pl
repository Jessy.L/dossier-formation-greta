Programme algo ;

VAR chaine1 : chaine ; 
VAR chaine2 : chaine ; 
VAR chaine3 : chaine ;

DEBUTPROG
	
	Afficher("Donner 2 chaine � saisir : ", CRLF);
	Saisir(chaine1);
	Saisir(chaine2);
	
	Afficher("Chaine1 : ", LONGUEUR(chaine1), CRLF);
	Afficher("Chaine2 : ", LONGUEUR(chaine2), CRLF);
	
	chaine3 <- CONCATENER(chaine1,chaine2);
	
	Afficher("Chaine 3 ", LONGUEUR(chaine3), " ", chaine3, CRLF);
	

FINPROG

Programme net_salaire ;

VAR vitesseKmh : entier = 0 ; 
CONSTANTE mSeconde : r�el = 0.277778 ;
CONSTANTE ktNoeuds : r�el = 0.539957 ;

DEBUTPROG
	
	Afficher("")
	
	Afficher("Tapez votre vitesse KM/H : ", CRLF);
	Saisir(vitesseKmh);
	
	
	
	Afficher("La vitesse ms : ", (vitesseKmh * mSeconde):4:2, CRLF);
	Afficher("La vitesse noeuds : ", (vitesseKmh * ktNoeuds):4:2, CRLF);

FINPROG

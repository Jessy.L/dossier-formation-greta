Programme algo ;

VAR nombre1 : r�el = 0 ;
VAR nombre2 : r�el = 0 ;

VAR operateur : entier = 1;

DEBUTPROG
	
	Afficher("Tapez les 2 nombre � calculer ", CRLF);
	Saisir(nombre1);
	Saisir(nombre2);

	Afficher("Choisir l'op�rateur :");
	Afficher(CRLF, "1 = +" , CRLF , "2 = -", CRLF , "3 = x", CRLF , "4 = /" , CRLF, "5 = Quitter");
	Saisir(operateur);


	SI(operateur = 1) ALORS 
		afficher(CRLF,"Add : ", (nombre1 + nombre2):4:2,CRLF);
	FINSI
	
	SI(operateur = 2) ALORS 
		afficher(CRLF,"Sous : ", (nombre1 - nombre2):4:2,CRLF);
	FINSI

	SI(operateur = 3) ALORS 
		afficher(CRLF,"Mult : ", (nombre1 * nombre2):4:2,CRLF);
	FINSI

	SI(operateur = 4) ALORS 
		SI (nombre1 = 0) OU (nombre2 = 0) ALORS
			Afficher(CRLF,"ZERO ERROR DIV",CRLF)
		SINON 
			afficher(CRLF,"Div : ", (nombre1 / nombre2):4:2,CRLF);
		FINSI	
	FINSI

	SI(operateur = 5) ALORS 
		EffacerEcran
	FINSI

FINCAS

FINPROG

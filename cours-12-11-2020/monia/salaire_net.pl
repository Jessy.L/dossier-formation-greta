Programme net_salaire ;

VAR salaire : entier = 0 ; 
CONSTANTE net : r�el = 0.1778 ;
VAR stock : r�el = 0 ;


DEBUTPROG
	Afficher("Tapez votre salaire : ", CRLF);
	Saisir(salaire);
	
	stock <- salaire*net;
	
	Afficher("Le salaire net : ", (salaire - stock):4:2, CRLF);

FINPROG

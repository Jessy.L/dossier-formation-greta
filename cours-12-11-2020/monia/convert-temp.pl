Programme algo ;

VAR temp : r�el = 0 ;
VAR fahra : r�el = 33.8 ;

DEBUTPROG
	
	Afficher("Saisir  une temp entre -50 et 50�C: ", CRLF);
	Saisir(temp);
	
	SI (temp > 50) ALORS

		Afficher(" VALEUR TROP GRANDE", CRLF);

	SINON SI (temp < -50) ALORS
			
			Afficher(" VALEUR TROP PETITE", CRLF);

		SINON 

			fahra <- (temp * 9/5) + 32 ;	
			Afficher( temp:4:2, " degr�s c -> ", fahra:4:2, " fahra", CRLF);	

		FINSI
	FINSI

FINPROG

@echo off
for /f "tokens=1-3 delims=/" %%f in ("%date%") do (
 set datation=%%f%%g%%h
 set jour=%%f
 set mois=%%g
 set année=%%h
)

Set /p cours1="Tapez le cours du matin :"
Set /p cours2="Tapez le cours du midi : "

md "cours-%jour%"-"%mois%"-"%année%"-"%cours1%"-"%cours2%"

cd "cours-%jour%"-"%mois%"-"%année%"-"%cours1%"-"%cours2%"

md "%cours1%"
md "%cours2%"
PROGRAMME nomduprog ;

VAR nombre : entier = 0 ;
	reponse : entier = 5 ;
	stock : entier = 0; 

DEBUTPROG
	
	afficher(" la r�ponse : (juste pour l'exemple) : ", reponse)

	TANT QUE (nombre <> reponse) FAIRE 
		afficher("Tapez le nombre ");
		saisir(nombre);
		stock <- stock + 1;

		SI stock = 10 ALORS 
			afficher(CRLF ,"Vous avez perdu ");
		FINSI
	
		SI nombre = reponse ALORS 
			afficher(CRLF , "Vous avez gagner ");
		FINSI
	
	FINTQ
	
FINPROG
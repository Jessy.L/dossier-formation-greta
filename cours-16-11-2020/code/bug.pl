PROGRAMME nomduprog ;

VAR age : entier = 0 ;
nom : caract�re ;
pr�nom : caract�re ;
stock : entier;

DEBUTPROG

    afficher("Entrez votre nom : ", CRLF) ;
    saisir(nom) ;
    stock <- ASCII(nom);
    afficher("Entrez votre pr�nom : ", CRLF) ;
    saisir(pr�nom) ;
    afficher("Entrez votre �ge : ", CRLF) ;
    saisir(age) ;
    afficher(CRLF,"Bonjour ", nom, " ", pr�nom, "!", "Vous avez ", (stock)," <- var stock |||| -> var AGE ", age, "ans ",CRLF) ; 

	SI ( age > 18 ) ALORS
	    afficher(CRLF,"Vous �tes majeur") ;

    SINON
        afficher(CRLF," Vous n'�tes pas majeur") ;

	FINSI

FINPROG
--
-- Table structure for table `Film`
--

DROP TABLE IF EXISTS `Film`;

CREATE TABLE `Film` (
  `idfilm` int(11) NOT NULL auto_increment,
  `titre` varchar(100) NOT NULL default '',
  `prenom_realisateur` varchar(50) NOT NULL default '',
  `nom_realisateur` varchar(50) NOT NULL default '',
  `genre` varchar(50) NOT NULL default '',
  `sortie` date NOT NULL default '0000-0-0',
  `recettes` int(20) NOT NULL default '0',
  PRIMARY KEY  (`idfilm`)
) ;

--
--  data for table `Film`
--


INSERT INTO `Film` VALUES (1,'Twilight chapitre II : Tentation','Chris','Weitz','Fantastique','2009-2-12',710),
(2,'Transformers','Michael','Bay','Fantastique','2007-6-6',709),
(3,'Twilight chapitre III : Hésitation','David','Slade','Fantastique','2010-1-10',698),
(4,'Forrest Gump','Robert','Zemeckis','Histoire','1994-5-20',677),
(5,'Avatar','James','Cameron','Fantastique','2009-2-15',2782),
(6,'Titanic','James','Cameron','Drame','1997-7-23',1843),
(7,'Harry Potter et les Reliques de la Mort II','David','Yates','Fantastique','2011-12-1',1294),
(8,'Le Seigneur des anneaux : Le Retour du roi','Peter','Jackson','Histoire','2003-5-23',1119),
(9,'Sixième Sens','Night','Shyamalan','Science fiction','1999-3-6',673),
(10,'L Âge de glace 2','Carlos','Saldanha','Animation','2006-9-6',655),
(11,'Pirates des Caraïbes : La Malédiction du Black Pearl','Gore','Verbinski','Fantastique','2003-4-2',654),
(12,'Transformers 3 : La Face cachée de la Lune','Michael','Bay','Fantastique','2011-4-23',1106),
(13,'Pirates des Caraïbes : Le Secret du coffre maudit','Gore','Verbinski','Fantastique','2006-12-1',1066),
(14,'Toy Story 3','Lee','Unkrich','Animation','2010-7-10',1063),
(15,'Pirates des Caraïbes : La Fontaine de Jouvence','Rob','Marshall','Fantastique','2011-3-9',1038),
(16,'Alice au pays des merveilles','Tim','Burton','Animation','2010-9-4',1024),
(17,'The Dark Knight : Le Chevalier noir','Christopher','Nolan','Fantastique','2008-8-6',1001),
(18,'Harry Potter à  l école des Sorciers','Chris','Columbus','Fantastique','2001-2-1',975),
(19,'Da Vinci Code','Ron','Howard','Policier','2006-8-2',758),
(20,'Shrek 4 : Il était une fin','Mike','Mitchell','Animation','2010-1-5',753),
(21,'Le Monde de Narnia','Andrew','Adamson','Fantastique','2005-7-4',745),
(22,'Matrix Reloaded','Andy','Wachowski','Science fiction','2003-9-21',742),
(23,'Là-haut','Bob','Peterson','Animation','2009-11-7',731),
(24,'Pirates des Caraïbes : Jusqu au bout du monde','Gore','Verbinski','Fantastique','2007-10-23',963),
(25,'Harry Potter et les Reliques de la Mort I','David','Yates','Fantastique','2010-4-3',955),
(26,'Harry Potter et l Ordre du Phénix','David','Yates','Fantastique','2007-2-1',940),
(27,'Harry Potter et le Prince de Sang-Mêlé','David','Yates','Fantastique','2009-9-4',934),
(28,'Le Seigneur des anneaux : Les Deux Tours','Peter','Jackson','Histoire','2002-7-12',925),
(29,'Star Wars I : La Menace fantôme','George','Lucas','Science fiction','1999-6-21',924),
(30,'Shrek 2','Andrew','Adamson','Animation','2004-5-26',920),
(31,'Independence Day','Roland','Emmerich','Science fiction','1996-1-2',817),
(32,'Shrek le troisième','Chris','Miller','Animation','2007-8-3',798),
(33,'Harry Potter et le Prisonnier d Azkaban','Alfonso','Cuaron','Fantastique','2003-4-18',796),
(34,'E.T. l extra-terrestre','Steven','Spielberg','Science fiction','1982-4-4',792),
(35,'Indiana Jones','Steven','Spielberg','Aventure','2008-2-2',786),
(36,'Le Roi lion','Roger','Allers','Animation','1994-6-6',784),
(37,'Spider-Man 2','Sam','Raimi','Fantastique','2004-11-23',783),
(38,'Star Wars  IV : Un nouvel espoir','George','Lucas','Science fiction','1977-9-17',775),
(39,'2012','Roland','Emmerich','Science fiction','2009-4-3',769),
(40,'L Âge de glace 3','Carlos','Saldanha','Animation','2009-9-1',886),
(41,'Harry Potter et la Chambre des Secrets','Chris','Columbus','Fantastique','2002-3-26',878),
(42,'Le Seigneur des anneaux : La Communauté de l anneau','Peter','Jackson','Histoire','2001-7-3',870),
(43,'Le Monde de Nemo','Andrew','Stanton','Animation','2003-5-1',867),
(44,'Star Wars III : La Revanche des Sith','George','Lucas','Science fiction','2005-11-4',848),
(45,'Transformers 2 : La Revanche','Michael','Bay','Fantastique','2009-8-2',836),
(46,'Inception','Christopher','Nolan','Fantastique','2010-9-4',825),
(47,'Spider-Man','Sam','Raimi','Fantastique','2002-6-6',821),
(48,'Jurassic Park','Steven','Spielberg','Fantastique','1993-8-12',914),
(49,'Harry Potter et la Coupe de Feu','Mike','Newell','Fantastique','2005-7-7',896),
(50,'Spider-Man 3','Sam','Raimi','Fantastique','2007-9-1',890);

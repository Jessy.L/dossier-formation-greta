const googleP = ["Chrome", 55]
const ieEdge =  ['IE + Edge', 7]
const safari =  ['Safari', 19]
const firefox = ['Firefox', 10]
const opera = ['Opera', 3]
const autres = ['Autres', 6]


google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Age', 'Weight'],
    [ 55,      55],
    [ 7,      7],
    [ 19,     19],
    [ 10,      10],
    [ 3,      3],
    [ 6,    6]
  ]);

  var options = {
    title: 'Age vs. Weight comparison',
    hAxis: {title: 'part', minValue: 0, maxValue: 100},
    vAxis: {title: 'part', minValue: 0, maxValue: 100},
    legend: 'none'
  };

  var chart = new google.visualization.ScatterChart(document.getElementById('div_nuage'));

  chart.draw(data, options);
}



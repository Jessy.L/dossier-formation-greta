
const googleP = ["Chrome", 55]
const ieEdge =  ['IE + Edge', 7]
const safari =  ['Safari', 19]
const firefox = ['Firefox', 10]
const opera = ['Opera', 3]
const autres = ['Autres', 6]

var type = 'corechart'
var stock = 0;

var btn = document.querySelectorAll('.btn')

btn.forEach(button => {
    
    button.addEventListener('click', function(){

      document.getElementById('piechart').innerHTML = ""
  
      if(this.dataset.value == "gauge"){
          type = 'gauge'
          stock = 2
      }

      if(this.dataset.value == "donut"){
          stock = 1
      }
  
      google.charts.load('current', {'packages':['gauge']});
      google.charts.setOnLoadCallback(drawChart);
    
    })
});

function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Part ', 'Part du marché'],
    [ieEdge[0] , ieEdge[1]],
    [safari[0] , safari[1]],
    [firefox[0], firefox[1]],
    [googleP[0], googleP[1]],
    [opera[0]  , opera[1]],
    [autres[0] , autres[1]],

  ]);

  if(stock == 1){
      var options = {
          
        title: 'Parts de marché des principaux navigateurs web',
        pieHole: 0.4, /* permet de faire le donut */
      };
  }else if(stock == 2){

      var options = {
        width: 500, height: 500,
        redFrom: 90, redTo: 100,
        yellowFrom:75, yellowTo: 90,
        minorTicks: 5
      };
  }else{

      var options = {
        title: 'Parts de marché des principaux navigateurs web'
      };
  }

  if(stock == 2){

    var chart = new google.visualization.Gauge(document.getElementById('piechart'));
  }else{

      var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  }

  
  chart.draw(data, options);
  stock = 0

}

/*VERSION DONUT*/

// var btn_donut = document.getElementById('btn_donut').addEventListener('click', function(){

//     google.charts.load("current", {packages:["corechart"]});
//     google.charts.setOnLoadCallback(donut);
    
// })

// function donut() {
//   var data = google.visualization.arrayToDataTable([
//       ['Part ', 'Part du marché'],
//       [ieEdge[0] , ieEdge[1]],
//       [safari[0] , safari[1]],
//       [firefox[0], firefox[1]],
//       [googleP[0], googleP[1]],
//       [opera[0]  , opera[1]],
//       [autres[0] , autres[1]],
  
//     ]);

//   var options = {
//     title: 'Parts de marché des principaux navigateurs web',
//     pieHole: 0.4, /* permet de faire le donut */
//   };

//   var chart = new google.visualization.PieChart(document.getElementById('donut'));
//   chart.draw(data, options);
// }


/*VERSION JAUGE*/


// google.charts.load('current', {'packages':['gauge']});
// google.charts.setOnLoadCallback(drawChart);

// function drawChart() {

//   var data = google.visualization.arrayToDataTable([
//     ['Label', 'Value'],
//     ['Memory', 80],
//     ['CPU', 55],
//     ['Network', 68]
//   ]);

//   var options = {
//     width: 400, height: 120,
//     redFrom: 90, redTo: 100,
//     yellowFrom:75, yellowTo: 90,
//     minorTicks: 5
//   };

//   var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

//   chart.draw(data, options);

//   setInterval(function() {
//     data.setValue(0, 1, 40 + Math.round(60 * Math.random()));
//     chart.draw(data, options);
//   }, 13000);
//   setInterval(function() {
//     data.setValue(1, 1, 40 + Math.round(60 * Math.random()));
//     chart.draw(data, options);
//   }, 5000);
//   setInterval(function() {
//     data.setValue(2, 1, 60 + Math.round(20 * Math.random()));
//     chart.draw(data, options);
//   }, 26000);
// }

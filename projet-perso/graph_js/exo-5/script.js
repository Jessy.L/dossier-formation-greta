


var tab = [];
var fichier = document.getElementById("fichier")
fichier.addEventListener('change', function (e) {
  const lecteur = new FileReader()

  lecteur.onload = function () {
    const lines = lecteur.result.split('\n').map(function (line) {
      return line.split(',')
    })

    for (i = 0; i < lines.length; i++) {
      tab.push(lines[i])
    }
  }
  lecteur.readAsText(fichier.files[0])
  setAPI()
}, false)


function setAPI(){
    google.charts.load('current', {'packages':['bar']});
    google.charts.setOnLoadCallback(drawChart);
    
}

function drawChart() {

  var data = google.visualization.arrayToDataTable()

//   var data = google.visualization.arrayToDataTable([
//     ['Year', 'Sales', 'Expenses', 'Profit'],
//     ['2014', 1000, 400, 200],
//     ['2015', 1170, 460, 250],
//     ['2016', 660, 1120, 300],
//     ['2017', 1030, 540, 350]
//   ]);

  data.addColumn('string');
  data.addColumn('number');

  for (let i = 1; i < data.length; i++) {
    data.addRow([(data[i][0]), (parseInt(data[i][1])),(parseInt(data[i][2])),(parseInt(data[i][3])),(parseInt(data[i][4])) ]);
  }

  var options = {
    chart: {
      title: 'Company Performance',
      subtitle: 'Sales, Expenses, and Profit: 2014-2017',
    }
  };

  var chart = new google.charts.Bar(document.getElementById('columnchart_material'));

  chart.draw(data, google.charts.Bar.convertOptions(options));
}

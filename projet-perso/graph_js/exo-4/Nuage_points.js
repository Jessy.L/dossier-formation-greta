
getData();

var table = []

async function getData() {
    const response = await fetch("Donnees.csv");
    const data = await response.text();

    const rows = data.split('\n');
    rows.forEach(element => {
        const row = element.split(',');
        const nav = row[0];
        const pourc = row[1];
        table.push(row);
    });
    console.log(table)
    setAPI()
}
function setAPI() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
}

function drawChart() {
  var data = google.visualization.arrayToDataTable([
    [table[0][0],   table[0][1]],
    [table[1][0],   Number(table[1][1])],
    [table[2][0],   Number(table[2][1])],
    [table[3][0],   Number(table[3][1])],
    [table[4][0],   Number(table[4][1])],
    [table[5][0],   Number(table[5][1])],
    [table[6][0],   Number(table[6][1])],
  ]);

  var options = {
    title: '% utilisation navigateur',
    hAxis: {title: 'navigateur', minValue: 0, maxValue: 15},
    vAxis: {title: '%', minValue: 0, maxValue: 15},
    legend: 'none'
  };

  var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

  chart.draw(data, options);
}
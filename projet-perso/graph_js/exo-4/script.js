var tab = [];
var fichier = document.getElementById("fichier")
fichier.addEventListener('change', function (e) {
  const lecteur = new FileReader()

  lecteur.onload = function () {
    const lines = lecteur.result.split('\n').map(function (line) {
      return line.split(',')
    })

    for (i = 0; i < lines.length; i++) {
      tab.push(lines[i])
    }
  }
  lecteur.readAsText(fichier.files[0])
  setAPI()
}, false)

function setAPI() {

  google.charts.load('current', { 'packages': ['corechart'] });
  google.charts.setOnLoadCallback(drawChart);
}

function drawChart() {

  var donnee = google.visualization.arrayToDataTable([

    [tab[0][0], tab[0][1]],
    [tab[1][0], Number(tab[1][1])],
    [tab[2][0], Number(tab[2][1])],
    [tab[3][0], Number(tab[3][1])],
    [tab[4][0], Number(tab[4][1])],
    [tab[5][0], Number(tab[5][1])],
    [tab[6][0], Number(tab[6][1])],

  ]);

  var options = {
    title: 'Parts de marché des principaux navigateurs web'
  };

  var chart = new google.visualization.PieChart(document.getElementById('box'));
  chart.draw(donnee, options);
}

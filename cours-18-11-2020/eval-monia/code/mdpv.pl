PROGRAMME nomduprog ;

Constante mdp : chaine = "TPdwwm2020" ;

VAR inputmdp : chaine ;
	compte : entier = 0;

 

DEBUTPROG
	  
	REPETER
   		Afficher("Tapez le mot de passe : ", CRLF);
   		saisir(inputmdp);
   		compte <- compte + 1 ; # incrémente les tentative
		
		SI comparer(mdp, inputmdp) = 0 ALORS # si mdp et input mdp = 0 alors mdp identique
   			afficher(CRLF,"BIENVENUE SUR LE SITE DU GRETA",CRLF);
  		SINON 
   			SI compte = 5 ALORS  # si les tentatives arive a 5 alors plus de tentative
   				afficher(CRLF,"PLUS DE TENTATIVES veuillez attendre 5 minutes",CRLF);
			 			  
  	  		SINON									
				afficher(CRLF, "MDP incorrect PLUS QUE ", 5 - compte ," TENTATIVE", CRLF ); # 5 - compte = tenta restante
			
			FINSI   		
   		FINSI   		
   		
   JUSQU'A CE QUE ( comparer(mdp, inputmdp) = 0) OU (compte = 5);
	
FINPROG

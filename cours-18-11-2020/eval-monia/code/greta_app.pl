Programme algo ;

VAR montant : r�el = 0 ;
VAR carte : entier = 0 ;

DEBUTPROG

	Afficher("Saisir le montant : ");
	Saisir(montant);
	
	# Si le montant est inf � 20 alors la condition s'applique 
	
	SI (montant < 20) ALORS 
		Afficher(CRLF,"Avez vous la carte de fid�litt� ?",CRLF,"1 = OUI",CRLF," 2 = NON :  ",CRLF);
		Saisir(carte);
		
		SI (carte = 1) ALORS
	 		Afficher("Le montant avec reduction est de : ", (montant * 0.95):4:2 ,CRLF);
		SINON
			Afficher("Vous devez payer ", montant ,CRLF);
		FINSI
	FINSI

	# SI le montant respecte certaine condition alors il va calculer q

	SI (montant >= 20) ET (montant < 50) ALORS Afficher(CRLF,"Le montant avec reduction est de : ", (montant * 0.90):4:6 ,CRLF); FINSI

	SI (montant >= 50) ET (montant < 100) ALORS  Afficher(CRLF,"Le montant avec reduction est de : ", (montant * 0.85):4:2 ,CRLF); FINSI

	SI (montant >= 100) ET (montant <= 500) ALORS  Afficher(CRLF,"Le montant avec reduction est de : ", (montant * 0.80):4:2 ,CRLF); FINSI


	# Si le montant est sup � 500 alors la condition s'applique 
	SI (montant > 500) ALORS 
	 	Afficher(CRLF,"Avez vous la carte de fid�litt� ?",CRLF,"1 = OUI",CRLF," 2 = NON :  ",CRLF);
		Saisir(carte);
		
		SI (carte = 1) ALORS
	 		Afficher("Le montant avec reduction est de : ", (montant * 0.95):4:2 ,CRLF);
		SINON
			Afficher("Vous devez payer ", montant ,CRLF);
		FINSI
	FINSI
	



FINPROG

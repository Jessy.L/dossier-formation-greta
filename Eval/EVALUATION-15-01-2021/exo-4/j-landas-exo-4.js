var verif =  true

var nom = document.getElementById('nom')
var prenom =  document.getElementById('prenom')
var email =  document.getElementById('email')
var identifiant =  document.getElementById('identifiant')
var password =  document.getElementById('password')
var cpassword =  document.getElementById('cpassword')


var message = document.getElementById('message')


var btn = document.getElementById('envoyer').addEventListener('click',function(){

    verif = true
    message.innerHTML = ""

    var stock_nom = String(nom.value)

    if(stock_nom.length < 3){
        
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Votre saisie du nom dois contenir plus de 3 caractère"
        verif = false
    }

    var stock_prenom = String(prenom.value)
    
    if(stock_prenom.length < 3){
        
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Votre saisie  du prenom dois contenir plus de 3 caractère"
        verif = false
   
    }

    var regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
    var adresseMail = email.value
    
    if (adresseMail.match(regex)){
        console.log("adresse mail ok")
    }
    else{
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Votre saisie d'adresse mail n'est pas un email"
        verif = false

    }

    var stock_identifiant = String(identifiant.value)


    if(stock_identifiant.length < 5){
        
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Votre saisie  de l'identifiant dois contenir plus de 5 caractère"
        verif = false
   
    }

    var stock_password = String(password.value)


    if(stock_password.length < 8){
        
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Votre saisie du mdp dois contenir plus de 8 caractère"
        verif = false
   
    }

    var stock_cpassword = String(cpassword.value)


    if(cpassword.value != password.value || stock_cpassword.length < 8){
        
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Vos mdp ne sont pas identique "   
        verif = false
    }

    if(verif == false){

        var all_p = document.querySelectorAll('p')
        all_p.forEach(element => {
            element.style.color = "red"
        });

        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Veuillez corrigez les erreurs de saisie"
    }else{
        var p = document.createElement('p')
        message.appendChild(p)
        p.innerHTML = "Tout est valide"
        p.style.color = "green"
    }

})

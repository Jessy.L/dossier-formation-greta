var input_string = document.getElementById('input_string')
input_string.addEventListener('blur', calcul)

var div = document.getElementById('content')

function calcul(){

    var stock = input_string.value
    stock = stock.split("")
    stock = stock.reverse()
    stock = stock.join("")

    var new_string = document.createElement('p')
    new_string.innerHTML = stock
    div.appendChild(new_string)

}
var affichage = [
    "Désignation",
    "REF",
    "PRIX HT",
    "Stock"
]

var article = [

    [
        "X700 Duality -light blue burst",
        92110,
        832.5,
        2
    ],

    [
        "American Ultra Telecaster",
        91852,
        1732.5,
        1
    ],

    [
        "LzzyHale Explorer Outfit Ltd",
        92013,
        581,
        2
    ],

    [
        "Kirk Hammett KH-602",
        78667,
        925,
        2
    ],

    [
        "Jimi Hendrix Stratocaster Voodoo Child",
        82359,
        3832,
        1
    ]

]

var exit = false

function menu(){

    while(exit != true){

        document.write("<table>")

        document.write("<thead>")
        for(i = 0; i < affichage.length; i++){
            document.write("<th>" + affichage[i] + "</th>")
        }
    
        document.write("</thead>")
        document.write("<tbody>")
    
        for(j = 0 ; j < article.length; j++){
    
            document.write("<tr>")
    
            for(k = 0; k < article[j].length; k++){
                document.write("<td>" + article[j][k] + "</td>")
            }
    
            document.write("</tr>")
    
        }
        document.write("</tbody>")
        document.write("</table>")


        document.write("<br><br> 1 Ajouter un élément <br>")
        document.write("2 Supprimer un élément / Rechercher un élément par référence <br>")
        document.write("3 Rechercher un élément par nom (même partiel) <br>")
        document.write("4 Trier par ordre de référence <br>")
        document.write("5 Trier par ordre de prix <br><br>")

        var choix = prompt("que voulez vous faire : ")

        if(choix == 1){


            var new_article = new Array()
            
            var nom_complet = prompt("Donner le nom complet de l'article : ")
            var ref = parseFloat(prompt("Donner la ref de l'article : "))
            var prix_ht = parseFloat(prompt("Donner le prix HT de l'article : "))
            var stock = parseFloat(prompt("Donner le stock de l'article : "))

            new_article.push(nom_complet, ref, prix_ht, stock)

            article.push(new_article)
            

        }else if(choix == 2){
      
            var reference_a_trouver = parseFloat(prompt("entrez la ref de l'article  : "))
            
            for(var l = 0; l <= article.length; l++){

                if(reference_a_trouver == article[l][1]){

                    var choix_supp = prompt("voulez vous le supprimer ? : 1 - Oui | 2 - Non ")

                    if(choix_supp == 1){
                        article.pop(l)
                    }else{
                        break;
                    }
                }
            }
            
        }else if(choix == 3){

            var recherche_tab = prompt("Entrez votre recherche : ")
            recherche_tab = recherche_tab.toLowerCase()

            var phrase = false


            for(p = 0; p < article.length ; p++){
                
                var conpare_chaine = article[p][0].toLowerCase()

                if(String(conpare_chaine).includes(recherche_tab) == true){
                    document.write(" Voici l'article en question : " + article[p][0] + " La ref  : " + article[p][1] + " Son prix HT  : " + article[p][2] + " le stock  : " + article[p][3] + "<br><br>")
                    phrase = true
                    return recherche_tab
                }
            }
            
            if(phrase == false){
                document.write("La recherche n'a pas abouti <br><br>")
            }

        }else if(choix == 4){
            
            var stock_all_ref = []
            var ref_add = 0
            var nb = 0
            
            document.write("<table>")
            
            
            for(q = 0; q < article.length; q++ ){
                stock_all_ref.push(article[q][1])
            }
            
            for(t = 0; t < affichage.length; t++){
                document.write("<th>" + affichage[t] + "</th>")
            }
            
            document.write("<tbody>")
            
            stock_all_ref = stock_all_ref.sort()

            console.log(stock_all_ref)

            do{
                console.log("_____________________________________")

                if(stock_all_ref[ref_add] == article[nb][1]){

                    console.log(stock_all_ref[ref_add] + "  FOR   " + article[nb][1])

                    document.write("<tr>")
    
                    for(s = 0; s < article[s].length; s++){
                        document.write("<td>" + article[nb][s] + "</td>")
                    }
            
                    document.write("</tr>")

                    ref_add++
                }

                console.log(stock_all_ref[ref_add] + " " + article[nb][1] + " " + nb + " " + ref_add)

                if(nb < 4 ){
                    nb++
                }else{
                    nb = 0
                }

            }while(ref_add < 5)

            document.write("</tbody>")
            document.write("</table> <br><br>")


        }else if(choix == 5){

            var stock_all_prix = []
            var prix_add = 0
            var nb = 0
            
            document.write("<table>")
            
            
            for(q = 0; q < article.length; q++ ){

                stock_all_prix.push(parseFloat(article[q][2]))
            }
            
            for(t = 0; t < affichage.length; t++){
                document.write("<th>" + affichage[t] + "</th>")
            }
            
            document.write("<tbody>")
            
            stock_all_prix = stock_all_prix.sort(function(a, b){return a - b})

            console.log(stock_all_prix)

            do{
                console.log("_____________________________________")

                if(stock_all_prix[prix_add] == article[nb][2]){

                    console.log(stock_all_prix[prix_add] + "  FOR   " + article[nb][2])

                    document.write("<tr>")
    
                    for(s = 0; s < article[s].length; s++){
                        document.write("<td>" + article[nb][s] + "</td>")
                    }
            
                    document.write("</tr>")

                    prix_add++
                }

                console.log(stock_all_prix[prix_add] + " " + article[nb][2] + " " + nb + " " + prix_add)

                if(nb < 4 ){
                    nb++
                }else{
                    nb = 0
                }

            }while(prix_add < 5)

            document.write("</tbody>")
            document.write("</table> <br><br>")



        }else{
            exit = true
        }
    }
}

menu()

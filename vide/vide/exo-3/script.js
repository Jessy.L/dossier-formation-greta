var newArticle = {};

function creationStorage(){

    if ( typeof localStorage != "undefined" && JSON) {

        var article = [

            [
                "X700 Duality -light blue burst",
                92110,
                832.5,
                2
            ],
        
            [
                "American Ultra Telecaster",
                91852,
                1732.5,
                1
            ],
        
            [
                "LzzyHale Explorer Outfit Ltd",
                92013,
                581,
                2
            ],
        
            [
                "Kirk Hammett KH-602",
                78667,
                925,
                2
            ],
        
            [
                "Jimi Hendrix Stratocaster Voodoo Child",
                82359,
                3832,
                1
            ]
        
        ]

        for(i = 0; i < article.length; i++ ){
                
            var ajout = {
            
                prepa : article[i][0],
                ref   : article[i][1],
                prix  : article[i][2],
                stock : article[i][3]
    
            };

            console.log(ajout)

            newArticle += ajout
        }

        localStorage.setItem("catalogue", JSON.stringify(ajout));
        return newArticle

    } else {
        alert("localStorage n'est pas supporté");
    }
}

function afficher(){

    var ajout = JSON.parse(localStorage.getItem("catalogue"));

    console.log(ajout)
}

function deleteAll(){
    localStorage.removeItem("catalogue");
}
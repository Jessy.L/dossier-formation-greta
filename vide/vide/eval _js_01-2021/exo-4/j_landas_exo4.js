var jour ;
var logement_souhaiter ;
var nombre_personne;
var prix ;

var entete = [
    "Type hébergement",
    "Tarif jour",
    "Tarif semaine"
]

var mobil = [

    "Standard 2 pers",
    "Standard 4 pers",
    "Standard 6 pers",

    "Luxe 2 pers",
    "Luxe 4 pers",
    "Luxe 6 pers",

]

var tarifs = [

    [30,200],
    [40,260],
    [50,280],

    [50,280],
    [60,330],
    [70,450]

]


document.write("<table>")
document.write("<thead>")

for(i = 0; i < entete.length; i++){

    document.write("<th>"+ entete[i] + "</th>")

}

document.write("</thead>")
document.write("<tbody>")

for(i = 0; i < mobil.length; i++ ){
    document.write("<tr>")
    
    document.write("<td>"+ mobil[i] +"</td>")
    for(j = 0; j < tarifs[i].length; j++){
        document.write("<td>"+ tarifs[i][j] +"</td>")
    }
    
    document.write("</tr>")

}

document.write("</tbody>")
document.write("</table>")



function saisie(){

    jour = parseFloat(prompt("Tapez le nombre de jour : "))
    logement_souhaiter = parseFloat(prompt("Le logement : 1 - Standard | 2 - Luxe"))
    nombre_personne = parseFloat(prompt("Le nombre de lit : 1 - 2 lit | 2- 4 lit | 3- 6 lit "))

    calculSejour(jour, logement_souhaiter, nombre_personne)
}

function calculSejour(jour, logement_souhaiter, nombre_personne){

    logement_souhaiter = logement_souhaiter

    var stock_semaine = Math.floor(jour / 7)
    console.log(stock_semaine)

    var stock_jour = jour%7
    console.log(stock_jour)

    if(logement_souhaiter == 1 && nombre_personne == 1 ){

        var logement = "Standard"
        var nbpersonne = "2 personnes"
        
        var stock_prix_jour = jour * 30; 
        var stock_prix_semaine = stock_semaine * 200 + stock_jour * 30;

        if(stock_prix_jour > stock_prix_semaine){
            prix = stock_prix_semaine;
        }else{
            prix = stock_prix_jour
        }

    }

    if(logement_souhaiter == 1 && nombre_personne == 2 ){

        var logement = "Standard"
        var nbpersonne = "4 personnes"
        
        var stock_prix_jour = jour * 40; 
        var stock_prix_semaine = stock_semaine * 260 + stock_jour * 40;

        if(stock_prix_jour > stock_prix_semaine){
            prix = stock_prix_semaine;
        }else{
            prix = stock_prix_jour
        }
    }

    if(logement_souhaiter == 1 && nombre_personne == 3 ){

        var logement = "Standard"
        var nbpersonne = "6 personnes"
     
        var stock_prix_jour = jour * 50; 
        var stock_prix_semaine = stock_semaine * 280 + stock_jour * 50;

        if(stock_prix_jour > stock_prix_semaine){
            prix = stock_prix_semaine;
        }else{
            prix = stock_prix_jour
        }
    }

    if(logement_souhaiter == 2 && nombre_personne == 1 ){

        var logement = "Luxe"
        var nbpersonne = "2 personnes"
        
        var stock_prix_jour = jour * 50; 
        var stock_prix_semaine = stock_semaine * 280 + stock_jour * 50;

        if(stock_prix_jour > stock_prix_semaine){
            prix = stock_prix_semaine;
        }else{
            prix = stock_prix_jour
        }

    }

    if(logement_souhaiter == 2 && nombre_personne == 2 ){

        var logement = "Luxe"
        var nbpersonne = "4 personnes"
        
        var stock_prix_jour = jour * 60; 
        var stock_prix_semaine = stock_semaine * 330 + stock_jour * 60;

        if(stock_prix_jour > stock_prix_semaine){
            prix = stock_prix_semaine;
        }else{
            prix = stock_prix_jour
        }
    }

    if(logement_souhaiter == 2 && nombre_personne == 3 ){
     
        var logement = "Luxe"
        var nbpersonne = "6 personnes"

        var stock_prix_jour = jour * 70; 
        var stock_prix_semaine = stock_semaine * 450 + stock_jour * 70;

        if(stock_prix_jour > stock_prix_semaine){
            prix = stock_prix_semaine;
        }else{
            prix = stock_prix_jour
        }
    }




    document.write("<br><br>")
    document.write("Votre saisie de jour : " + jour)
    document.write("<br><br>")
    document.write("Nombre de jour : " + stock_jour + "<br>") 
    document.write("Nombre de semaine : " + stock_semaine)
    document.write("<br><br>")
    document.write("Le prix de la pour la location : " + logement + " " + nbpersonne + " est au prix le plus avantageux de : " + prix + " €" )
}

saisie()
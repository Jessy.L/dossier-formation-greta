-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 05 fév. 2021 à 13:20
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

-- -------------------------------------
-- Jessy Landas
-- -------------------------------------

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `carteselectroniques`
--

-- --------------------------------------------------------

--
-- Structure de la table `carte`
--

DROP TABLE IF EXISTS `carte`;
CREATE TABLE IF NOT EXISTS `carte` (
  `idCartes` int(11) NOT NULL AUTO_INCREMENT,
  `CodeBarre` varchar(45) NOT NULL,
  `Cartecol` varchar(45) NOT NULL,
  `Commentaire` varchar(100) NOT NULL,
  `DateTest` date NOT NULL,
  `DateAjoutBase` date NOT NULL,
  PRIMARY KEY (`idCartes`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `carte`
--

INSERT INTO `carte` (`idCartes`, `CodeBarre`, `Cartecol`, `Commentaire`, `DateTest`, `DateAjoutBase`) VALUES
(1, '59500', '', 'Delevoye', '2021-02-04', '2021-02-05'),
(2, 'RZ234BA3-0006', '', 'VISI80', '2020-10-07', '2020-10-16'),
(3, 'RZ234BA3-0011', '', 'VISI80', '2020-11-27', '2020-12-08'),
(4, 'RZ234BA3-0008', '', 'VISI49 ', '2019-04-03', '2019-04-10'),
(5, 'RZ234BA3-0035', '', 'VISI37', '2018-09-10', '2018-09-19'),
(6, 'RZ234BA3-0048', '', 'VISI37', '2017-06-10', '2017-06-19'),
(7, 'RZ234BA3-0013', '', 'VISI37', '2016-05-21', '2016-05-19');

-- --------------------------------------------------------

--
-- Structure de la table `emplacement`
--

DROP TABLE IF EXISTS `emplacement`;
CREATE TABLE IF NOT EXISTS `emplacement` (
  `idEmplacement` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idEmplacement`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `emplacement`
--

INSERT INTO `emplacement` (`idEmplacement`, `Nom`) VALUES
(1, 'Delevoye'),
(2, 'Magasin Réception'),
(3, 'PosteTest'),
(4, 'MagasinRéserve'),
(5, 'Sur Site');

-- --------------------------------------------------------

--
-- Structure de la table `localisationcartes`
--

DROP TABLE IF EXISTS `localisationcartes`;
CREATE TABLE IF NOT EXISTS `localisationcartes` (
  `idLocalisationCartes` int(11) NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `idCartes` int(11) NOT NULL,
  `idEmplacement` int(11) NOT NULL,
  PRIMARY KEY (`idLocalisationCartes`),
  KEY `idCartes` (`idCartes`) USING BTREE,
  KEY `idEmplacement` (`idEmplacement`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `localisationcartes`
--

INSERT INTO `localisationcartes` (`idLocalisationCartes`, `Date`, `idCartes`, `idEmplacement`) VALUES
(1, '2021-02-05', 1, 1),
(2, '2021-02-05', 3, 2),
(3, '2021-02-05', 3, 5),
(4, '2021-02-05', 2, 4),
(5, '2021-02-05', 2, 3),
(6, '2021-02-05', 4, 5),
(7, '2021-02-05', 4, 5),
(8, '2021-02-05', 5, 3),
(9, '2021-02-05', 5, 2),
(10, '2021-02-05', 6, 5),
(11, '2021-02-05', 6, 2),
(12, '2021-02-05', 7, 4),
(13, '2021-02-05', 7, 3);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `localisationcartes`
--
ALTER TABLE `localisationcartes`
  ADD CONSTRAINT `localisationcartes_ibfk_1` FOREIGN KEY (`idCartes`) REFERENCES `carte` (`idCartes`),
  ADD CONSTRAINT `localisationcartes_ibfk_2` FOREIGN KEY (`idEmplacement`) REFERENCES `emplacement` (`idEmplacement`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
